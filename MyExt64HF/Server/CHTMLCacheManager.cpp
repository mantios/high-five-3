
#include <Server/CHTMLCacheManager.h>
#include <Common/Utils.h>

namespace l2server {

void CHTMLCacheManager::Init()
{
	WriteInstructionCall(0x447214 + 0x121, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x4474BC + 0x121, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x4EB1D4 + 0xED, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x4EDEA0 + 0x130, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x5972C0 + 0x233D, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x671004 + 0xE8, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x6E3798 + 0xA4, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x7BF038 + 0x184, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x7F1164 + 0xB3, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x7FF084 + 0xC6, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x88BA9C + 0xB1, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x88BC04 + 0xB7, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0x8C84D4 + 0x23D, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0xA00E38 + 0x295, FnPtr(&CHTMLCacheManager::GetHTMLFile));
	WriteInstructionCall(0xA1DDC8 + 0x395, FnPtr(&CHTMLCacheManager::GetHTMLFile));
}

bool CHTMLCacheManager::GetHTMLFile(const wchar_t *filename, xstd::wstring &output, int language)
{
	// Check for \ / and ..
	bool wasDot = false;
	for (size_t i = 0 ; filename[i] ; ++i) {
		if (filename[i] == L'\\' || filename[i] == L'/') return false;
		if (filename[i] == L'.') {
			if (wasDot) return false;
			wasDot = true;
		} else {
			wasDot = false;
		}
	}

	// Call original GetHTMLFile
	return reinterpret_cast<bool(*)(CHTMLCacheManager*, const wchar_t*, xstd::wstring&, int)>(0x6D8F2C)(this, filename, output, language);
}

} // namespace l2server

