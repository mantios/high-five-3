
#include <Server/CUserBroadcaster.h>
#include <Common/Utils.h>

namespace l2server {

void CUserBroadcaster::Init()
{
	WriteInstructionJmp(FnPtr(&CUserBroadcaster::BroadcastToAllUser), 0x9D8948);
}

CUserBroadcaster* CUserBroadcaster::Instance()
{
	if (!(*reinterpret_cast<UINT32*>(0x12D9D4C) & 1)) {
		*reinterpret_cast<UINT32*>(0x12D9D4C) |= 1;
		*reinterpret_cast<UINT32*>(0x12D9D48) = 0;
	}
	return reinterpret_cast<CUserBroadcaster*>(0x12D9D48);
}

void CUserBroadcaster::BroadcastToAllUser(const char *format, ...)
{
	throw std::runtime_error("Not initialized!");
}

} // namespace l2server

