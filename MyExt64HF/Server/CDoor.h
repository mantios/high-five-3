
#pragma once

#include <Server/CStaticObject.h>

namespace l2server {

class CDoor : public CStaticObject {
public:
	static const UINT64 vtable = 0xC223D8;
};

} // namespace l2server

