
#pragma once

#include <Common/xstd.h>

namespace l2server {

class User;

class CMultiPartyCommandChannel {
public:
	void SendRelationUpdates();
	xstd::vector<User*> GetAllMember();
};

} // namespace l2server

