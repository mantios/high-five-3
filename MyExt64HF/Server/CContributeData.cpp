
#include <Server/CContributeData.h>
#include <Common/Utils.h>

namespace l2server {

void CContributeData::Init()
{
	NOPMemory(0x7D7A6F, 20); // Fix potential race condition
}

void CContributeData::Clear()
{
	reinterpret_cast<void(*)(CContributeData*)>(0x7DA774)(this);
	topContributorId = 0;
	topContributorLevel = 0;
	topDamage = 0;
}

bool CContributeData::PickableIn5Sec(CCreature *creature)
{
	return reinterpret_cast<bool(*)(CContributeData*, CCreature*)>(0x7D7974)(this, creature);
}

static void check()
{
	static_assert(offsetof(CContributeData, data) == 0x0060);
	static_assert(offsetof(CContributeData, looterId) == 0x0078);
}

} // namespace l2server

