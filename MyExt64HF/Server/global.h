
#pragma once

#include <Common/xstd.h>

namespace l2server {

class RWLock;

namespace global {

// Auth session lock
extern class l2server::RWLock *authSessionLock;

// Auth account -> session map
extern xstd::map<int, int> *authAccountSessionMap;

// Font check
extern int &fontcheck;

} // namespace global
} // namespace l2server

