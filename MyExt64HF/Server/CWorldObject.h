
#pragma once

#include <Server/CObject.h>

namespace l2server {

class CWorldObject : public CObject {
public:
	static const UINT64 vtable = 0xBC69D8;

	/* 0x04A8 */ virtual void vfn0x04A8() {}
	/* 0x04B0 */ virtual FVector ConvRelToAbs_0x04B0(FVector f) { return FVector(); }
	/*        */ inline FVector ConvRelToAbs(FVector f) { return ConvRelToAbs_0x04B0(f); }
	/* 0x04B8 */ virtual FVector ConvAbsToRel_0x04B8(FVector f) { return FVector(); }
	/*        */ inline FVector ConvAbsToRel(FVector f) { return ConvAbsToRel_0x04B8(f); }
	/* 0x04C0 */ virtual void vfn0x04C0(double) {}
	/* 0x04C8 */ virtual double vfn0x04C8() { return 0.0; }
	/* 0x04D0 */ virtual void vfn0x04D0(double) {}
	/* 0x04D8 */ virtual bool IsInWorld_0x04D8() { return false; }
	/*        */ inline bool IsInWorld() { return IsInWorld_0x04D8(); }
	/* 0x04E0 */ virtual void vfn0x04E0() {}
	/* 0x04E8 */ virtual void vfn0x04E8() {}

	/* 0x0040 */ unsigned char padding0x0040[0x0058 - 0x0040];
};

} // namespace l2server

