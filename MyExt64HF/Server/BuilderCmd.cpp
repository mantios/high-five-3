
#include <Server/BuilderCmd.h>
#include <Server/BuilderCmd2Handler.h>
#include <Server/CUserSocket.h>
#include <Server/User.h>
#include <Server/CUserSocketReleaseVerifier.h>
#include <Common/Utils.h>

namespace l2server {

void BuilderCmd_Init()
{
	// Initialize custom builder command handler
	BuilderCmd2Handler::Init();

	// Kick that works for offline users
	WriteInstructionCall(0x504C6B, FnPtr(BuilderCmd_kick_helper));
}

void BuilderCmd_kick_helper(CUserSocket *socket)
{
	if (User *user = socket->user) {
		if (user->CloseOfflineTrade()) return;
	}
	reinterpret_cast<void(*)(CUserSocket*)>(0x45D41C)(socket);
}

} // namespace l2server

