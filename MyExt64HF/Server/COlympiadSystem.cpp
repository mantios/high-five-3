
#pragma once

#include <Server/COlympiadSystem.h>
#include <Common/Utils.h>
#include <Common/Config.h>
#include <time.h>

namespace l2server {

UINT32 &COlympiadSystem::olympiadStartTime = *reinterpret_cast<UINT32 * const>(0x1258CEEC);

void COlympiadSystem::Init()
{
	// Force monthly olympiad
	WriteInstructionCall(0x50AE24 + 0x7B4, FnPtr(&COlympiadSystem::SetOlympiadTerm));
	WriteInstructionCall(0x50AE24 + 0x7F5, FnPtr(&COlympiadSystem::SetOlympiadTerm));
	WriteInstructionCall(0x50AE24 + 0xB4E, FnPtr(&COlympiadSystem::SetOlympiadTerm));
	WriteInstructionCall(0x50AE24 + 0xB9A, FnPtr(&COlympiadSystem::SetOlympiadTerm));
	WriteInstructionCall(0x6565D0 + 0x102, FnPtr(&COlympiadSystem::SetOlympiadTerm));
	WriteInstructionCall(0x7EC9D0 + 0x158, FnPtr(&COlympiadSystem::SetOlympiadTerm));
	WriteInstructionCall(0x7EFB88 + 0xBE, FnPtr(&COlympiadSystem::SetOlympiadTerm));
}

void COlympiadSystem::SetOlympiadTerm(bool monthly, bool unknown1, int period, int startSec, int bonus1, int bonus2, int bonus3, int bonus4, int nominate, bool unknown2)
{
	if (Config::Instance()->fixes->forceMonthlyOlympiad) {
		const time_t now = time(0);
		struct tm t;
		localtime_s(&t, &now);
		t.tm_mday = 1;
		t.tm_hour = 0;
		t.tm_min = 0;
		t.tm_sec = 0;
		const time_t startTime = mktime(&t);
		if (++t.tm_mon > 11) {
			t.tm_mon = 0;
			++t.tm_year;
		}
		const time_t nextMonthStart = mktime(&t);
		this->olympiadStartTime = startTime;
		monthly = true;
		nominate = nextMonthStart - startTime;
	}
	reinterpret_cast<void(*)(COlympiadSystem*, bool, bool, int, int, int, int, int, int, int, bool)>(0x7EC7B8)(
		this, monthly, unknown1, period, startSec, bonus1, bonus2, bonus3, bonus4, nominate, unknown2);
}

} // namespace l2server

