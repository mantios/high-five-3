
#pragma once

#include <Server/CStaticObject.h>

namespace l2server {

class CControlTower : public CStaticObject {
public:
	static const UINT64 vtable = 0xBC3C68;
};

} // namespace l2server

