
#pragma once

#include <Common/Enum.h>

namespace l2server {

class CItem;

class CSkillInfo {
public:
	static void Init();
	static bool CanEnchantAttribute(AttributeTypeEnum type, CItem *item);
	void GetConflictingAttributes(bool *conflictingAttributes);
	bool IsValidTarget(class CCreature *attacker, class CCreature *target, bool b) const;

	unsigned char padding0x0000[0x0378 - 0x0000];
	bool enchantAttributeTypes[6];
};

} // namespace server

