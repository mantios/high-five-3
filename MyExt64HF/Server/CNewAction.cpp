
#include <Server/CNewAction.h>
#include <Server/CCreature.h>
#include <Common/CSharedCreatureData.h>
#include <Common/Utils.h>

namespace l2server {

void CAttackAction::Init()
{
	WriteInstructionCall(0x77E15A, FnPtr(CheckAttackValidity));
}

bool CAttackAction::CheckAttackValidity(CCreature *attacker, CCreature *target)
{
	GUARDED;

	if (target && target->IsBoss() && target->objectType == 1029028) { // valakas
		if (abs(attacker->sd->location.z - target->sd->location.z) > 128) {
			return false;
		}
	}
	return reinterpret_cast<bool(*)(CCreature*, CCreature*)>(0x779A2C)(attacker, target);
}

} // namespace l2server

