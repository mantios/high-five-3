
#include <Server/CWareHouseDeposit.h>
#include <Common/Utils.h>

namespace l2server {

void CWareHouseDeposit::Init()
{
	WriteMemoryBYTES(0xA2DBCD, "\x8B\x48\x08", 3); // mov ecx, dword ptr [rax+8]
	WriteInstructionCallJmpEax(0xA2DBD0, FnPtr(SendListHelper));
}

UINT64 CWareHouseDeposit::SendListHelper(int type)
{
	switch (type) {
	case 2:
	case 0x10:
	case 0x1A:
		return 0xA2DBD8;
	default:
		return 0xA2DC2D;
	}
}

} // namespace l2server

