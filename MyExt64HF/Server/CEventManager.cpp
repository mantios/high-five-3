
#pragma once

#include <Server/CEventManager.h>
#include <Server/CEvent.h>
#include <Common/Config.h>
#include <Common/Utils.h>

namespace l2server {

void CEventManager::Init()
{
	WriteInstructionCall(0x47DED0+0x11, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51E0DC+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51E194+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51E24C+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51E304+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51E534+0xFB, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51E6F0+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51E7A8+0x8F, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51E880+0x8F, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51E958+0x8F, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51EB4C+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51EC04+0x1E8, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51EEEC+0x163, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51EEEC+0x283, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51F258+0xD0, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51F504+0x8B, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51F85C+0xCD, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51F9A4+0x9C, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51FB50+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51FC78+0xA5, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x51FE20+0xD3, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x52011C+0x8B, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x520C10+0x2A2, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x5FF5A8+0x147, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x5FFBB4+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x5FFC6C+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x5FFD24+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x5FFDDC+0x84, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x5FFEB4+0x84, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x600314+0x14B, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x67A1DC+0xBB, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x67A640+0x7AF, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x6A9354+0x1BA, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D5A44+0x1F4, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D5D6C+0xC0, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D5E74+0x8C, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D5F4C+0x8C, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D6024+0x97, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D612C+0x89, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D6214+0xA1, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D66B0+0x9A, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D67A8+0xEB, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D6934+0x114, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x7D6BB4+0x6C, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x920684+0xD6, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x98E8BC+0x171, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x98EB14+0x124, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x98ECBC+0xC0, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x98EFA4+0x19, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x98F010+0x6C, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x98F170+0x74, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x98F288+0x82, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x98F500+0x8E, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x98FF9C+0x124, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x9902D4+0xC7, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x990660+0x71, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x990728+0x79, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x990954+0x7C, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x990B06, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x993D64, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x99466C+0x78, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x994864+0x7F, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x995058+0x167, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x9961C5, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x99625E, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x9EEB94+0x8F, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0x9EEC6C+0xC3, FnPtr(&CEventManager::GetEvent));
	WriteInstructionCall(0xA22660+0x71, FnPtr(&CEventManager::GetEvent));
}

CEvent* CEventManager::GetEvent(GAME_EVENT::EVENT_TYPE eventType)
{
	if (Config::Instance()->custom->disableNavitSystem && eventType == GAME_EVENT::VOTE_SYSTEM) return 0;
	return reinterpret_cast<CEvent*(*)(CEventManager*, GAME_EVENT::EVENT_TYPE)>(0x69BE0C)(this, eventType);
}

} // namespace l2server

