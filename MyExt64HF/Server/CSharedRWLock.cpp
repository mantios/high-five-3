
#include <Server/CSharedRWLock.h>
#include <Common/Utils.h>

namespace l2server {

void CSharedRWLock::ReadLock(bool concession)
{
	reinterpret_cast<void(*)(CSharedRWLock*, bool)>(0x8BE444)(this, concession);
}

void CSharedRWLock::WriteLock()
{
	reinterpret_cast<void(*)(CSharedRWLock*)>(0x8BE59C)(this);
}

void CSharedRWLock::Done()
{
	reinterpret_cast<void(*)(CSharedRWLock*)>(0x8BE6D4)(this);
}

CSharedRWLockReadGuard::CSharedRWLockReadGuard(CSharedRWLock *lock)
	: lock(lock), locked(true)
{
	lock->ReadLock(false);
}

CSharedRWLockReadGuard::~CSharedRWLockReadGuard()
{
	if (locked) {
		lock->Done();
	}
}

void CSharedRWLockReadGuard::Lock()
{
	if (locked) return;
	lock->ReadLock(false);
	locked = true;
}

void CSharedRWLockReadGuard::Unlock()
{
	if (!locked) return;
	lock->Done();
	locked = false;
}

CSharedRWLockWriteGuard::CSharedRWLockWriteGuard(CSharedRWLock *lock)
	: lock(lock), locked(true)
{
	lock->WriteLock();
}

CSharedRWLockWriteGuard::~CSharedRWLockWriteGuard()
{
	if (locked) {
		lock->Done();
	}
}

void CSharedRWLockWriteGuard::Lock()
{
	if (locked) return;
	lock->WriteLock();
	locked = true;
}

void CSharedRWLockWriteGuard::Unlock()
{
	if (!locked) return;
	lock->Done();
	locked = false;
}

} // namespace l2server

