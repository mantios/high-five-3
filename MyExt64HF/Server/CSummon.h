
#pragma once

#include <Server/CNPC.h>

namespace l2server {

class CSummon : public CNPC {
public:
	static const UINT64 vtable = 0xCEBFE8;

	static void Init();

	bool IsEnemyToWrapper(CCreature *creature);
};

} // namespace l2server

