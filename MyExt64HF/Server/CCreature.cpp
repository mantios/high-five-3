
#include <Server/CCreature.h>
#include <Server/CItem.h>
#include <Server/CObjectDB.h>
#include <Server/NpcServer.h>
#include <Server/CCreatureController.h>
#include <Server/CNewAction.h>
#include <Server/CSummon.h>
#include <Server/CPet.h>
#include <Server/CMerchant.h>
#include <Server/CBoss.h>
#include <Server/CZzoldagu.h>
#include <Server/CNPC.h>
#include <Server/User.h>
#include <Server/CContributeData.h>
#include <Server/CParty.h>
#include <Server/CObjectDB.h>
#include <Common/CSharedCreatureData.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/Config.h>
#include <Common/CSPointer.h>
#include <Common/xstd.h>

template<>
CSPointer<l2server::CCreature> CSafePointer2<l2server::CCreature>::FindObjectSP()
{
	if (!objectId) return CSPointer<l2server::CCreature>();
	return CSPointer<l2server::CCreature>(
		reinterpret_cast<l2server::CCreature*>(
			l2server::CObjectDB::GetObject(objectId)), 7);
}

namespace l2server {

void CCreature::Init()
{
	// Out of sight wrapper - hide bug fix
	WriteMemoryQWORD(CCreature::vtable + 0x478, FnPtr(&CCreature::OutOfSightWrapper));
	WriteMemoryQWORD(CNPC::vtable + 0x478, FnPtr(&CCreature::OutOfSightWrapper));
	WriteMemoryQWORD(CSummon::vtable + 0x478, FnPtr(&CCreature::OutOfSightWrapper));
	WriteMemoryQWORD(CPet::vtable + 0x478, FnPtr(&CCreature::OutOfSightWrapper));
	WriteMemoryQWORD(CBoss::vtable + 0x478, FnPtr(&CCreature::OutOfSightWrapper));
	WriteMemoryQWORD(CZzoldagu::vtable + 0x478, FnPtr(&CCreature::OutOfSightWrapper));
	WriteMemoryQWORD(CMerchant::vtable + 0x478, FnPtr(&CCreature::OutOfSightWrapper));
	WriteMemoryQWORD(User::vtable + 0x478, FnPtr(&CCreature::OutOfSightWrapper));

	// Fix skill reuse time missing ceil()
	WriteInstructionCall(0x5ADC80 + 0xB3, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x5AEB98 + 0xC0, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x5B1050 + 0x697, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x78FFC4 + 0x7D, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x79010C + 0xA0, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x8E4948 + 0xA4, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x8F7E7C + 0xA3, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x8F7E7C + 0xCB, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x907004 + 0xE7, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x90AB0C, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x94F484 + 0x79, FnPtr(&CCreature::GetRemainReuseDelaySec));
	WriteInstructionCall(0x96C58C + 0xE96, FnPtr(&CCreature::GetRemainReuseDelaySec));

	// Epilogue attribute bonuses
	WriteInstructionCall(0x59C14D, FnPtr(&CCreature::GetBonusBoundary));

	// Allow skills on airship if configured so
	WriteInstructionCall(0x56B826, FnPtr(&CCreature::OnMagicSkillUseIsOnAirship), 0x56B82F);

	// Buff and song/dance slots
	WriteMemoryBYTE(0x5A5924 + 4, static_cast<unsigned char>(Config::Instance()->custom->maxNormalBuffSlots));
	WriteMemoryBYTE(0x5A5967 + 2, static_cast<unsigned char>(Config::Instance()->custom->maxNormalBuffSlots));
	WriteMemoryBYTE(0x5A5975 + 4, static_cast<unsigned char>(Config::Instance()->custom->maxSongDanceSlots));
}

void CCreature::SendSystemMessageFmt(const wchar_t *sender, const wchar_t *format, ...)
{
	va_list args;
	wchar_t buffer[4096];
	va_start(args, format);
	vswprintf_s(buffer, sizeof(buffer) / sizeof(buffer[0]), format, args);
	va_end(args);
	SendSystemMessage(sender, buffer);
}

bool CCreature::AddItemToInventory(int itemType, INT64 amount, ItemGetReason reason, int enchant, int unknown1, int unknown2, const ItemOptionKey &option, int unknown3, const BaseAttribute &attribute)
{
	return reinterpret_cast<bool(*)(CCreature*, int, INT64, ItemGetReason, int, int, int, const ItemOptionKey&, int, const BaseAttribute&)>(0x568E6C)(
		this, itemType, amount, reason, enchant, unknown1, unknown2, option, unknown3, attribute);
}

bool CCreature::AddItemToInventory(int itemType, INT64 amount, int enchant, const ItemOptionKey &option, const BaseAttribute &attribute)
{
	return AddItemToInventory(itemType, amount, ItemGetReasonUnknown, enchant, 0, 0, option, 0, attribute);
}

bool CCreature::AddItemToInventory(int itemType, INT64 amount, int enchant)
{
	ItemOptionKey option;
	BaseAttribute attribute;
	return AddItemToInventory(itemType, amount, enchant, option, attribute);
}

CSPointer<CObject> CCreature::GetTarget()
{
	return CSPointer<CObject>(CObjectDB::Instance()->GetObject(targetId));
}

void CCreature::OutOfSightWrapper(CObject *object, bool b)
{
	GUARDED;

	if (object->IsCreature()) {
		// Creature disappeared for us
		CCreature *creature = object->CastCreature();

		// Did we have it in target?
		if (targetId == creature->objectId) {
			// Yes, change target to none
			ChangeTarget(0, ChangeTargetReasonSkill);

			// Am I a summon or a pet?
			if (IsSummonOrPet()) {
				// Yes, do stop pet action
				NpcServer::Instance()->Send("cdd", NpcPacket::PET_ACTION, objectId, PetActionStop);
			}
		}

		// Are we non-user and is creature user?
		if (!IsUser() && creature->IsUser()) {
			// Check if we have creature in target
			if (targetId == creature->objectId) {
				// Yes, send do nothing
				NpcServer::Instance()->Send("cd", NpcPacket::DO_NOTHING, objectId);
			}

			// Remove hate (forget there was user)
			NpcServer::Instance()->Send("cdddd", NpcPacket::REMOVE_HATE, objectId, sd->index, creature->objectId, creature->objectId);
		}

		// Get current action
		CNewAction *action = GetCreatureController()->GetCurrentAction();

		// If there is any action
		if (action) {
			// Depending on action type
			switch (GetVt(action)) {
			case CAttackAction::vtable:
				// Attack, check target
				if (reinterpret_cast<CAttackAction*>(action)->targetId == creature->objectId) {
					// Stop attack
					DoNothing();
				}
				break;
			case CSkillAction2::vtable:
				// Skill, check target
				if (reinterpret_cast<CSkillAction2*>(action)->targetId == creature->objectId) {
					// Stop skill casting
					DoNothing();
				}
				break;
			}
		}
	}

	// Only user has OutOfSight implemented, check if we're user
	if (IsUser()) {
		// Yes, call original method
		reinterpret_cast<void(*)(CCreature*, CObject*, bool)>(0x9652FC)(this, object, b);
	}
}

void CCreature::DoNothing()
{
	reinterpret_cast<void(*)(CCreature*)>(0x574CC0)(this);
}

unsigned long CCreature::GetSkillUsedTime(const int skillId)
{
	return reinterpret_cast<unsigned long(*)(CCreature*, const int)>(0x5892F4)(this, skillId);
}

int CCreature::GetRemainReuseDelaySec(const int skillId)
{
	GUARDED;

	UINT64 skillUsedTime = GetSkillUsedTime(skillId);
	if (!skillUsedTime) {
		return 0;
	}

	INT64 delta = skillUsedTime - GetTickCount();
	if (delta <= 0) {
		return 0;
	}
	if (Config::Instance()->custom->dontFixReuseBug) return floor(double(delta) / 1000.0);
	return ceil(double(delta) / 1000.0);
}

void CCreature::GetBonusBoundary(int difference, double *bonus1, double *bonus2)
{
	GUARDED;

	if (Config::Instance()->custom->epilogueAttributeBonuses) {
		if (difference < -300) {
			*bonus1 = 40.0;
			*bonus2 = -80.0;
		} else if (difference < -150) {
			*bonus1 = 40.0;
			*bonus2 = -60.0;
		} else if (difference < 150) {
			*bonus1 = 40.0;
			*bonus2 = -50.0;
		} else if (difference < 300) {
			*bonus1 = 70.0;
			*bonus2 = -50.0;
		} else {
			*bonus1 = 100.0;
			*bonus2 = -50.0;
		}
	} else {
		*bonus2 = 0.0;
		if (difference < 150) *bonus1 = 20.0;
		else if (difference < 300) *bonus1 = 40.0;
		else *bonus1 = 70.0;
	}
}

bool CCreature::OnMagicSkillUseIsOnAirship()
{
	if (Config::Instance()->server->allowSkillsOnAirship) return false;
	return IsOnAirship();
}

void CCreature::GatherNeighborCreature(FVector position, int type, int limit, bool unknown,
                                       xstd::vector<CSafePointer2<CCreature> > &targets,
                                       xstd::vector<double> &distances,
                                       int range, int heightMin, int heightMax,
                                       SkillAffectObjectTypeEnum affectObjectType,
                                       CCreature *caster,
                                       CCreature *excluded,
                                       bool unknown2,
                                       bool unknown3)
{
	reinterpret_cast<void(*)(
		CCreature*, FVector, int, int, bool,
		xstd::vector<CSafePointer2<CCreature> >&,
		xstd::vector<double>&,
		int, int, int,
		SkillAffectObjectTypeEnum,
		CCreature*, CCreature*,
		bool, bool)>(0x56EDF0)(
			this, position, type, limit, unknown,
			targets,
			distances,
			range, heightMin, heightMax,
			affectObjectType,
			caster, excluded,
			unknown2, unknown3);
}

static void check()
{
	static_assert(offsetof(CCreature, sd) == 0xA88);
	static_assert(offsetof(CCreature, inventory) == 0xAA0);
	static_assert(offsetof(CCreature, padding0x0B70) == 0xB70);
	static_assert(offsetof(CCreature, targetId) == 0x15D0);
}

} // namespace l2server

