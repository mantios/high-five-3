
#include <Server/CPet.h>
#include <Server/User.h>

namespace l2server {

void CPet::Init()
{
	// Pet expoff
	WriteMemoryQWORD(0xCEE038, FnPtr(&CPet::ExpIncEx));
}

INT64 CPet::ExpIncEx(INT64 exp, bool b)
{
	User *user(GetMaster());
	if (user && user->ext.petExpOff && exp > 0) exp = 0;
	return reinterpret_cast<INT64(*)(CPet*, INT64, bool)>(0x82F15C)(this, exp, b);
}

} // namespace l2server

