
#pragma once

#include <Windows.h>

namespace l2server {

// Shutdown checker - periodically reads status.txt file
// If it finds "shutdown" there, initiates server shutdown
class ShutdownChecker {
public:
	static void Init();
	static void Start();
	static void CheckForShutdown();
};

} // namespace server

