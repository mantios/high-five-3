
#include <Server/CCursedWeaponConstInfo.h>

namespace l2server {

CCursedWeaponInfo* CCursedWeaponConstInfo::Find(int classId)
{
	return reinterpret_cast<CCursedWeaponInfo*(*)(CCursedWeaponConstInfo*, int)>(0x5B3334)(this, classId);
}

} // namespace l2server

