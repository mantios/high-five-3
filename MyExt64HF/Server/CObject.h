
#pragma once

#include <Server/CIOObject.h>
#include <Common/Utils.h>

namespace l2server {

class CCreature;
class CPet;
class User;
class CNPC;
class CStaticObject;
class CItem;

class CObject : public CIOObject {
public:
	static const UINT64 vtable = 0xCC88B8;

	/* 0x0060 */ virtual bool SetVisible_0x0060(const bool visible) {}
	/*        */ inline bool SetVisible(const bool visible) { return SetVisible_0x0060(visible); }
	/* 0x0068 */ virtual bool IsVisible_0x0068() { return false; }
	/*        */ inline bool IsVisible() { return IsVisible_0x0068(); }
	/* 0x0070 */ virtual void vfn0x0070() {}
	/* 0x0078 */ virtual void vfn0x0078() {}
	/* 0x0080 */ virtual void vfn0x0080() {}
	/* 0x0088 */ virtual void vfn0x0088() {}
	/* 0x0090 */ virtual void vfn0x0090() {}
	/* 0x0098 */ virtual void vfn0x0098() {}
	/* 0x00A0 */ virtual int GetObjectType_0x00A0() { return 0; }
	/*        */ inline int GetObjectType() { return GetObjectType_0x00A0(); }
	/* 0x00A8 */ virtual void GetInstantZone_0x00A8() {}
	/* 0x00B0 */ virtual int GetInZoneID_0x00B0() { return 0; }
	/*        */ inline int GetInZoneID() { return GetInZoneID_0x00B0(); }
	/* 0x00B8 */ virtual void SetInZoneID_0x00B8() {}
	/* 0x00C0 */ virtual void vfn0x00C0() {}
	/* 0x00C8 */ virtual void vfn0x00C8() {}
	/* 0x00D0 */ virtual void vfn0x00D0() {}
	/* 0x00D8 */ virtual void vfn0x00D8() {}
	/* 0x00E0 */ virtual void vfn0x00E0() {}
	/* 0x00E8 */ virtual int GetLevel_0x00E8() { return 0; }
	/*        */ inline int GetLevel() { return GetLevel_0x00E8(); }
	/* 0x00F0 */ virtual FVector GetPosition_0x00F0() { return FVector(); }
	/*        */ inline FVector GetPosition() { return GetPosition_0x00F0(); }
	/* 0x00F8 */ virtual void vfn0x00F8() {}
	/* 0x0100 */ virtual void vfn0x0100() {}
	/* 0x0108 */ virtual void vfn0x0108() {}
	/* 0x0110 */ virtual void vfn0x0110() {}
	/* 0x0118 */ virtual void vfn0x0118() {}
	/* 0x0120 */ virtual void vfn0x0120() {}
	/* 0x0128 */ virtual void vfn0x0128() {}
	/* 0x0130 */ virtual void vfn0x0130() {}
	/* 0x0138 */ virtual void vfn0x0138() {}
	/* 0x0140 */ virtual void vfn0x0140() {}
	/* 0x0148 */ virtual void vfn0x0148() {}
	/* 0x0150 */ virtual void vfn0x0150() {}
	/* 0x0158 */ virtual void vfn0x0158() {}
	/* 0x0160 */ virtual bool IsCreature_0x0160() const { return false; }
	/*        */ inline bool IsCreature() const { return IsCreature_0x0160(); }
	/* 0x0168 */ virtual bool IsUser_0x0168() const { return false; }
	/*        */ inline bool IsUser() const { return IsUser_0x0168(); }
	/* 0x0170 */ virtual bool IsNpc_0x0170() const { return false; }
	/*        */ inline bool IsNpc() const { return IsNpc_0x0170(); }
	/* 0x0178 */ virtual bool IsPet_0x0178() const { return false; }
	/*        */ inline bool IsPet() const { return IsPet_0x0178(); }
	/* 0x0180 */ virtual void vfn0x0180() {}
	/* 0x0188 */ virtual bool IsSummonOrPet_0x0188() const { return false; }
	/*        */ inline bool IsSummonOrPet() const { return IsSummonOrPet_0x0188(); }
	/* 0x0190 */ virtual void vfn0x0190() {}
	/* 0x0198 */ virtual bool IsBoss_0x0198() const { return false; }
	/*        */ inline bool IsBoss() const { return IsBoss_0x0198(); }
	/* 0x01A0 */ virtual bool IsZzoldagu_0x01A0() const { return false; }
	/*        */ inline bool IsZzoldagu() const { return IsZzoldagu_0x01A0(); }
	/* 0x01A8 */ virtual void vfn0x01A8() {}
	/* 0x01B0 */ virtual void vfn0x01B0() {}
	/* 0x01B8 */ virtual void vfn0x01B8() {}
	/* 0x01C0 */ virtual void vfn0x01C0() {}
	/* 0x01C8 */ virtual void vfn0x01C8() {}
	/* 0x01D0 */ virtual void vfn0x01D0() {}
	/* 0x01D8 */ virtual void vfn0x01D8() {}
	/* 0x01E0 */ virtual void vfn0x01E0() {}
	/* 0x01E8 */ virtual void vfn0x01E8() {}
	/* 0x01F0 */ virtual CCreature* CastCreature_0x1F0() { return 0; }
	/*        */ inline CCreature* CastCreature() { return CastCreature_0x1F0(); }
	/* 0x01F8 */ virtual void vfn0x01F8() {}
	/* 0x0200 */ virtual CPet* CastPet_0x0200() { return 0; }
	/*        */ inline CPet* CastPet() { return CastPet_0x0200(); }
	/* 0x0208 */ virtual User* CastUser_0x0208() { return 0; }
	/*        */ inline User* CastUser() { return CastUser_0x0208(); }
	/* 0x0210 */ virtual CNPC* CastNpc_0x0210() { return 0; }
	/*        */ inline CNPC* CastNpc() { return CastNpc_0x0210(); }
	/* 0x0218 */ virtual void vfn0x0218() {}
	/* 0x0220 */ virtual void vfn0x0220() {}
	/* 0x0228 */ virtual void vfn0x0228() {}
	/* 0x0230 */ virtual void vfn0x0230() {}
	/* 0x0238 */ virtual void vfn0x0238() {}
	/* 0x0240 */ virtual void vfn0x0240() {}
	/* 0x0248 */ virtual void vfn0x0248() {}
	/* 0x0250 */ virtual void vfn0x0250() {}
	/* 0x0258 */ virtual User* GetUserOrMaster_0x0258() { return 0; }
	/*        */ inline User* GetUserOrMaster() { return GetUserOrMaster_0x0258(); }
	/* 0x0260 */ virtual bool IsItem_0x0260() const { return false; }
	/*        */ inline bool IsItem() const { return IsItem_0x0260(); }
	/* 0x0268 */ virtual bool IsWeapon_0x0268() const { return false; }
	/*        */ inline bool IsWeapon() const { return IsWeapon_0x0268(); }
	/* 0x0270 */ virtual bool IsArmor_0x0270() const { return false; }
	/*        */ inline bool IsArmor() const { return IsArmor_0x0270(); }
	/* 0x0278 */ virtual bool IsAccessary_0x0278() const { return false; }
	/*        */ inline bool IsAccessary() const { return IsAccessary_0x0278(); }
	/* 0x0280 */ virtual void vfn0x0280() {}
	/* 0x0288 */ virtual void vfn0x0288() {}
	/* 0x0290 */ virtual void vfn0x0290() {}
	/* 0x0298 */ virtual CItem* CastItem_0x0298() { return 0; }
	/*        */ inline CItem* CastItem() { return CastItem_0x0298(); }
	/* 0x02A0 */ virtual bool IsStaticObject_0x02A0() { return false; }
	/*        */ inline bool IsStaticObject() { return IsStaticObject_0x02A0(); }
	/* 0x02A8 */ virtual void vfn0x02A8() {}
	/* 0x02B0 */ virtual void vfn0x02B0() {}
	/* 0x02B8 */ virtual void vfn0x02B8() {}
	/* 0x02C0 */ virtual void vfn0x02C0() {}
	/* 0x02C8 */ virtual void vfn0x02C8() {}
	/* 0x02D0 */ virtual void vfn0x02D0() {}
	/* 0x02D8 */ virtual void vfn0x02D8() {}
	/* 0x02E0 */ virtual void vfn0x02E0() {}
	/* 0x02E8 */ virtual void vfn0x02E8() {}
	/* 0x02F0 */ virtual void vfn0x02F0() {}
	/* 0x02F8 */ virtual void vfn0x02F8() {}
	/* 0x0300 */ virtual void vfn0x0300() {}
	/* 0x0308 */ virtual void vfn0x0308() {}
	/* 0x0310 */ virtual void vfn0x0310() {}
	/* 0x0318 */ virtual void vfn0x0318() {}
	/* 0x0320 */ virtual void vfn0x0320() {}
	/* 0x0328 */ virtual void vfn0x0328() {}
	/* 0x0330 */ virtual void vfn0x0330() {}
	/* 0x0338 */ virtual void vfn0x0338() {}
	/* 0x0340 */ virtual void vfn0x0340() {}
	/* 0x0348 */ virtual CStaticObject* CastStaticObject_0x0348() { return 0; }
	/*        */ inline CStaticObject* CastStaticObject() { return CastStaticObject_0x0348(); }
	/* 0x0350 */ virtual void vfn0x0350() {}
	/* 0x0358 */ virtual void vfn0x0358() {}
	/* 0x0360 */ virtual void vfn0x0360() {}
	/* 0x0368 */ virtual void vfn0x0368() {}
	/* 0x0370 */ virtual void vfn0x0370() {}
	/* 0x0378 */ virtual void vfn0x0378() {}
	/* 0x0380 */ virtual void vfn0x0380() {}
	/* 0x0388 */ virtual void vfn0x0388() {}
	/* 0x0390 */ virtual void vfn0x0390() {}
	/* 0x0398 */ virtual void vfn0x0398() {}
	/* 0x03A0 */ virtual void vfn0x03A0() {}
	/* 0x03A8 */ virtual void vfn0x03A8() {}
	/* 0x03B0 */ virtual void vfn0x03B0() {}
	/* 0x03B8 */ virtual void vfn0x03B8() {}
	/* 0x03C0 */ virtual void vfn0x03C0() {}
	/* 0x03C8 */ virtual void vfn0x03C8() {}
	/* 0x03D0 */ virtual void vfn0x03D0() {}
	/* 0x03D8 */ virtual void vfn0x03D8() {}
	/* 0x03E0 */ virtual void vfn0x03E0() {}
	/* 0x03E8 */ virtual void vfn0x03E8() {}
	/* 0x03F0 */ virtual bool IsOnAirship_0x03F0() const { return false; }
	/*        */ inline bool IsOnAirship() const { return IsOnAirship_0x03F0(); }
	/* 0x03F8 */ virtual void vfn0x03F8() {}
	/* 0x0400 */ virtual int GetAirshipId_0x0400() { return 0; }
	/*        */ inline int GetAirshipId() { return GetAirshipId_0x0400(); }
	/* 0x0408 */ virtual bool IsRealEnemyTo_0x0408(CCreature *creature) { return false; }
	/*        */ inline bool IsRealEnemyTo(CCreature *creature) { return IsRealEnemyTo_0x0408(creature); }
	/* 0x0410 */ virtual bool IsEnemyTo_0x0410(CCreature *creature) { return false; }
	/*        */ inline bool IsEnemyTo(CCreature *creature) { return IsEnemyTo_0x0410(creature); }
	/* 0x0418 */ virtual void vfn0x0418() {}
	/* 0x0420 */ virtual void vfn0x0420() {}
	/* 0x0428 */ virtual void vfn0x0428() {}
	/* 0x0430 */ virtual bool IsAlive_0x0430() const { return false; }
	/*        */ inline bool IsAlive() const { return IsAlive_0x0430(); }
	/* 0x0438 */ virtual void vfn0x0438() {}
	/* 0x0440 */ virtual void vfn0x0440() {}
	/* 0x0448 */ virtual void vfn0x0448() {}
	/* 0x0450 */ virtual void vfn0x0450() {}
	/* 0x0458 */ virtual void vfn0x0458() {}
	/* 0x0460 */ virtual void vfn0x0460() {}
	/* 0x0468 */ virtual bool IsRiding_0x0468() const {}
	/*        */ inline bool IsRiding() const { return IsRiding_0x0468(); }
	/* 0x0470 */ virtual void vfn0x0470() {}
	/* 0x0478 */ virtual void OutOfSight_0x0478(CObject *object, bool b) {}
	/*        */ inline void OutOfSight(CObject *object, bool b) { OutOfSight_0x0478(object, b); }
	/* 0x0480 */ virtual void vfn0x0480() {}
	/* 0x0488 */ virtual const wchar_t* GetName_0x0488() { return 0; }
	/*        */ inline const wchar_t* GetName() { return GetName_0x0488(); }
	/* 0x0490 */ virtual int GetDBID_0x0490() { return 0; }
	/*        */ inline int GetDBID() { return GetDBID_0x0490(); }
	/* 0x0498 */ virtual void vfn0x0498() {}
	/* 0x04A0 */ virtual void vfn0x04A0() {}

	/* 0x0018 */ int objectId;
	/* 0x001C */ int objectType;
	/* 0x0020 */ unsigned char padding0x0020[0x0040 - 0x0020];
	/* 0x0040 */
};

} // namespace l2server

