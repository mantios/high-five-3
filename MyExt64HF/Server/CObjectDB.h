
#pragma once

#include <Windows.h>
#include <Common/CSPointer.h>

#ifdef GetObject
#undef GetObject
#endif // GetObject

namespace l2server {

class CObjectDB {
public:
	static CObjectDB* Instance();

	int GetClassIdFromName(const wchar_t *name) const;
	class CItem* CreateItem(const int itemType) const;
	class CItem* GetItem(const int id);

	static class CObject* GetObject(const int id);

	template<class T>
	static CSPointer<T> GetObjectByIndex(const int index)
	{
		CSPointer<T> result;
		GetObjectByIndex(&result, index);
		return result;
	}

	template<class T>
	static void GetObjectByIndex(CSPointer<T> *result, const int index)
	{
		if (*result) result->Reset();
		reinterpret_cast<void(*)(UINT64, CSPointer<T>*, const int)>(0x44796C)(0x110AD90, result, index);
	}

	template<class T>
	static CSPointer<T> GetObjectByDBID(const int dbId)
	{
		CSPointer<T> result;
		GetObjectByDBID(&result, dbId);
		return result;
	}

	template<class T>
	static void GetObjectByDBID(CSPointer<T> *result, const int dbId)
	{
		if (*result) result->Reset();
		reinterpret_cast<void(*)(UINT64, CSPointer<T>*, std::pair<int, int>)>(0x451120)(0x13F66A30, result, std::make_pair(GetGlobalWorldId(), dbId));
	}

	template<class T>
	static CSPointer<T> GetObjectByAccountID(const int accountId)
	{
		CSPointer<T> result;
		GetObjectByAccountID(&result, accountId);
		return result;
	}

	template<class T>
	static void GetObjectByAccountID(CSPointer<T> *result, const int accountId)
	{
		if (*result) result->Reset();
		reinterpret_cast<void(*)(UINT64, CSPointer<T>*, int)>(0x4605CC)(0x14006A40, result, accountId);
	}
};

} // namespace l2server

