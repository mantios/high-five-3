
#include <Server/CYieldLock.h>
#include <Common/Utils.h>

namespace l2server {

CYieldLock::CYieldLock()
{
	reinterpret_cast<CYieldLock*(*)(CYieldLock*)>(0xA4CCD4)(this);
}

CYieldLock::~CYieldLock()
{
}

void CYieldLock::Enter(const wchar_t *filename, const unsigned int line)
{
	reinterpret_cast<void(__thiscall*)(CYieldLock*, const wchar_t*, const unsigned int)>(0xA4CCEC)(this, filename, line);
}

void CYieldLock::Leave(const wchar_t *filename, const unsigned int line)
{
	reinterpret_cast<void(__thiscall*)(CYieldLock*, const wchar_t*, const unsigned int)>(0xA4CD8C)(this, filename, line);
}

CYieldLockGuard::CYieldLockGuard(CYieldLock *lock)
	: lock(lock), locked(true)
{
	lock->Enter(__FILEW__, __LINE__);
}

CYieldLockGuard::~CYieldLockGuard()
{
	if (locked) {
		lock->Leave(__FILEW__, __LINE__);
	}
}

void CYieldLockGuard::Lock()
{
	if (locked) return;
	lock->Enter(__FILEW__, __LINE__);
	locked = true;
}

void CYieldLockGuard::Unlock()
{
	if (!locked) return;
	lock->Leave(__FILEW__, __LINE__);
	locked = false;
}

} // namespace l2server

