
#include <Server/CItem.h>

namespace l2server {

void CItem::Init()
{
	// Ext
	WriteMemoryDWORD(0x710B4B + 1, sizeof(CItem));
	WriteMemoryDWORD(0x7195D1 + 1, sizeof(CItem));
	WriteMemoryDWORD(0xA6D553 + 1, sizeof(CItem));
	WriteMemoryDWORD(0xA6D59B + 1, sizeof(CItem));
	WriteMemoryDWORD(0x719459 + 1, sizeof(CItem));
	WriteMemoryDWORD(0xA6D50F + 1, sizeof(CItem));
	WriteMemoryDWORD(0x7192E1 + 1, sizeof(CItem));
	WriteMemoryDWORD(0xA6D5E3 + 1, sizeof(CItem));
	WriteMemoryDWORD(0x719749 + 1, sizeof(CItem));
	WriteMemoryDWORD(0xA6D617 + 1, sizeof(CItem));
	WriteMemoryDWORD(0x7198C1 + 1, sizeof(CItem));
	WriteMemoryDWORD(0xA6D4CB + 1, sizeof(CItem));
	WriteMemoryDWORD(0x719A39 + 1, sizeof(CItem));
	WriteMemoryDWORD(0x7096C6 + 8, sizeof(CItem));
	WriteInstructionCall(0x719125, FnPtr(&CItem::Constructor));
	WriteInstructionCall(0xA6D4EA, FnPtr(&CItem::Constructor));
	WriteInstructionCall(0xA6D52E, FnPtr(&CItem::Constructor));
	WriteInstructionCall(0xA6D572, FnPtr(&CItem::Constructor));
	WriteInstructionCall(0xA6D5BA, FnPtr(&CItem::Constructor));
	WriteInstructionCall(0xA6D636, FnPtr(&CItem::Constructor));
	WriteInstructionCall(0x710A8C, FnPtr(&CItem::CopyConstructor));
	WriteInstructionCall(0x710B67, FnPtr(&CItem::CopyConstructor));
	WriteInstructionCall(0x7191C9, FnPtr(&CItem::CopyConstructor));
	WriteInstructionCall(0x7192FE, FnPtr(&CItem::CopyConstructor));
	WriteInstructionCall(0x719476, FnPtr(&CItem::CopyConstructor));
	WriteInstructionCall(0x7195EE, FnPtr(&CItem::CopyConstructor));
	WriteInstructionCall(0x7198DE, FnPtr(&CItem::CopyConstructor));
	WriteInstructionCall(0x719A56, FnPtr(&CItem::CopyConstructor));
	WriteMemoryQWORD(0xC7C398, FnPtr(&CItem::Destructor));
	WriteMemoryQWORD(0xC7BDF8, FnPtr(&CItem::Destructor));
	WriteMemoryQWORD(0xC7B858, FnPtr(&CItem::Destructor));
	WriteMemoryQWORD(0xC7DCA8, FnPtr(&CItem::Destructor));
	WriteMemoryQWORD(0xC7C958, FnPtr(&CItem::Destructor));
	WriteMemoryQWORD(0xC7CEF8, FnPtr(&CItem::Destructor));
}

ItemOptionKey::ItemOptionKey() : option1(0), option2(0)
{
}

BaseAttribute::BaseAttribute() : type(0), value(-2), defenseFire(0), defenseWater(0),
defenseWind(0), defenseEarth(0), defenseHoly(0), defenseUnholy(0), unknown(0)
{
}

CItem* CItem::Constructor(bool unknown)
{
	CItem *result = reinterpret_cast<CItem*(*)(CItem*, bool)>(0x7102B8)(this, unknown);
	new (&result->ext) Ext();
	return result;
}

CItem* CItem::CopyConstructor(const CItem &other)
{
	CItem *result = reinterpret_cast<CItem*(*)(CItem*, const CItem&)>(0x71065C)(this, other);
	new (&result->ext) Ext(ext);
	return result;
}

void* CItem::Destructor(bool freeMemory)
{
	ext.~Ext();
	return reinterpret_cast<void*(*)(CItem*, bool)>(0x7191F0)(this, freeMemory);
}

CItem::Ext::Ext() : autolooted(false), autolootedAsync(false)
{
}

CItem::Ext::~Ext()
{
}

CContributeData* CItem::GetContributeData()
{
	return reinterpret_cast<CContributeData*(*)(unsigned int*)>(0x9259B4)(&contributeDataObjectId);
}

static void check()
{
	static_assert(offsetof(CItem, baseAttribute) == 0x120);
	static_assert(offsetof(CItem, padding0x0132) == 0x132);
	static_assert(offsetof(CItem, ext) == 0x158);

	static_assert(offsetof(CItem::ItemInfo, forceEquip) == 0x136);
	static_assert(offsetof(CItem::ItemInfo, crystalType) == 0x158);
	static_assert(sizeof(CItem::ItemInfo) == 0x1A0);
}

} // namespace l2server

