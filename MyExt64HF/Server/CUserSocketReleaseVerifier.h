
#pragma once

#include <Server/CIOObject.h>

namespace l2server {

class CUserSocket;
class User;

class CUserSocketReleaseVerifier : public CIOObject {
public:
	// Static initialization
	static void Init();

	// Treat offline trade users same way as users in combat
	static UINT64 OfflineTradeCheckHelper(User *user);

	// Don't set timer for offline trade users, just store pointer to verifier for later use
	void OfflineTradeTimerHelper(const int interval, const int id);

	/* 0x0018 */ CUserSocket *socket;
};

} // namespace l2server

