
#pragma once

#include <Windows.h>

class FVector;
class CSharedItemData;

namespace l2server {

class User;
class CNPC;
class CItem;
class CObject;
class CCreature;

class CWorld {
public:
	static void Init();

	static CWorld* Instance();

	bool PutItemNPCDrop(CItem *item, FVector &pos, CNPC *npc);
	bool PutItem(CItem *item, FVector &pos, CNPC *npc);
	bool PickItem(CItem *item, User *user);
	static UINT64 PickItemIsInWorldHelper(CItem *item);
	static UINT64 PickItemIsInWorldHelper2(CItem *item);
	static void AsyncPickLogHelper(void *logInstance, int type, const char *format, CItem *item, const char *file, int line);
	static UINT64 PickEffectHelper(CCreature *creature, CItem *item);
};

} // namespace l2server

