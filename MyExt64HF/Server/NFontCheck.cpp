
#include <Server/NFontCheck.h>

namespace l2server {

NFontCheck* NFontCheck::Instance()
{
	return reinterpret_cast<NFontCheck*>(0x4BF8058);
}

bool NFontCheck::CheckCode(int code)
{
	return reinterpret_cast<bool(*)(NFontCheck*, int)>(0x67B87C)(this, code);
}

} // namespace l2server
