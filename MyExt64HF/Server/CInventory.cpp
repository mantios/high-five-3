
#include <Server/CInventory.h>
#include <Server/CObjectDB.h>
#include <Server/User.h>
#include <Common/Utils.h>

namespace l2server {

void CInventory::Init()
{
	WriteInstructionCall(0x703C36, FnPtr(GetObjectTradeFix));
}

void CInventory::GetObjectTradeFix(void*, CSPointer<CObject> &ptr, UINT32 index)
{
	new (&ptr) CSPointer<CCreature>(); // it's not initialized
	CObjectDB::GetObjectByIndex(&ptr, index);
	if (!ptr) return;
	if (!ptr->IsUser()) return;
	User *user = reinterpret_cast<User*>(&*ptr);
	if (Guard::WasCalled(reinterpret_cast<wchar_t*>(0xD78310))) return; // "bool __cdecl CTrade::Canceled(class User *)"
	if (!user->IsNowTrade()) return;
	if (Guard::WasCalled(reinterpret_cast<wchar_t*>(0xC17CE0))) return; // "bool __cdecl DBPacketHandler::ReplyTrade(class CDBSocket *,const unsigned char *)"
	if (Guard::WasCalled(reinterpret_cast<wchar_t*>(0xC0EF70))) return; // "bool __cdecl ManipulateItemPacket(class CDBSocket *,const unsigned char *)"
	if (Guard::WasCalled(reinterpret_cast<wchar_t*>(0xD78460))) return; // "int __cdecl CTrade::AddItems(const unsigned char *,unsigned int)"
	if (Guard::WasCalled(reinterpret_cast<wchar_t*>(0xDA21E0))) return; // "int __cdecl User::DecreaseEquippedItemDurationOnTimerExpired(void)"
	user->TradeCancel();
}

bool CInventory::CheckAddable(int itemType, INT64 amount, bool unknown)
{
	return reinterpret_cast<bool(*)(CInventory*, int, INT64, bool)>(0x701EDC)(this, itemType, amount, unknown);
}

} // namespace l2server

