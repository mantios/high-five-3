
#pragma once

#include <Windows.h>

namespace l2server {

class COlympiadSystem {
public:
	static UINT32 &olympiadStartTime;

	static void Init();
	void SetOlympiadTerm(bool monthly, bool unknown1, int period, int startSec, int bonus1, int bonus2, int bonus3, int bonus4, int nominate, bool unknown2);
};

} // namespace l2server

