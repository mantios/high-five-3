
#pragma once

#include <Server/MemoryObject.h>

namespace l2server {

class CIOObject : public MemoryObject {
public:
	static const UINT64 vtable = 0xC71C18;

	void AddTimer(const int interval, const int id);

	/* 0x0020 */ virtual void TimerExpired(const int id) {}
	/* 0x0028 */ virtual void DeliverTimerExpired(const int id) {}
	/* 0x0030 */ virtual void vfn0x0030() {}
	/* 0x0038 */ virtual void vfn0x0038() {}
	/* 0x0040 */ virtual void vfn0x0040() {}
	/* 0x0048 */ virtual void vfn0x0048() {}
	/* 0x0050 */ virtual void vfn0x0050() {}
	/* 0x0058 */ virtual void vfn0x0058() {}
};

} // namespace l2server

