
#include <Server/RWLock.h>
#include <Common/Utils.h>

namespace l2server {

void RWLock::ReadLock(const wchar_t *file, const int line)
{
	reinterpret_cast<void(*)(RWLock*, const wchar_t*, const int)>(0x8AAA4C)(this, file, line);
}

void RWLock::ReadUnlock()
{
	reinterpret_cast<void(*)(RWLock*)>(0x8AABDC)(this);
}

void RWLock::WriteLock(const wchar_t *file, const int line)
{
	reinterpret_cast<void(*)(RWLock*, const wchar_t*, const int)>(0x8AACEC)(this, file, line);
}

void RWLock::WriteUnlock()
{
	reinterpret_cast<void(*)(RWLock*)>(0x8BE6D4)(this);
}

RWLockReadGuard::RWLockReadGuard(RWLock *lock)
	: lock(lock), locked(true)
{
	lock->ReadLock(__FILEW__, __LINE__);
}

RWLockReadGuard::~RWLockReadGuard()
{
	if (!locked) return;
	lock->ReadUnlock();
}

void RWLockReadGuard::Lock()
{
	if (locked) return;
	lock->ReadLock(__FILEW__, __LINE__);
	locked = true;
}

void RWLockReadGuard::Unlock()
{
	if (!locked) return;
	lock->ReadUnlock();
	locked = false;
}

RWLockWriteGuard::RWLockWriteGuard(RWLock *lock)
	: lock(lock), locked(true)
{
	lock->WriteLock(__FILEW__, __LINE__);
}

RWLockWriteGuard::~RWLockWriteGuard()
{
	if (!locked) return;
	lock->WriteUnlock();
}

void RWLockWriteGuard::Lock()
{
	if (locked) return;
	lock->WriteLock(__FILEW__, __LINE__);
	locked = true;
}

void RWLockWriteGuard::Unlock()
{
	if (!locked) return;
	lock->WriteUnlock();
	locked = false;
}

} // namespace l2server

