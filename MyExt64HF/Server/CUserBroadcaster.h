
#pragma once

namespace l2server {

class CUserBroadcaster {
public:
	static void Init();
	static CUserBroadcaster* Instance();
	void BroadcastToAllUser(const char *format, ...);
};

} // namespace l2server

