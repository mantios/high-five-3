
#pragma once

#include <Windows.h>

namespace l2server {

class CWareHouseDeposit {
public:
	static void Init();
	static UINT64 SendListHelper(int type);
};

} // namespace l2server

