
#pragma once

#include <windows.h>

namespace l2server {

// RWLock used on many places
class RWLock {
public:
	void ReadLock(const wchar_t *file, const int line);
	void ReadUnlock();
	void WriteLock(const wchar_t *file, const int line);
	void WriteUnlock();
};

// Scoped guard (read) for RWLock
class RWLockReadGuard {
private:
	RWLockReadGuard();
	RWLockReadGuard(const RWLockReadGuard &other);
public:
	RWLockReadGuard(RWLock *lock);
	~RWLockReadGuard();
	void Unlock();
	void Lock();
protected:
	RWLock *lock;
	bool locked;
};

// Scoped guard (write) for RWLock
class RWLockWriteGuard {
private:
	RWLockWriteGuard();
	RWLockWriteGuard(const RWLockWriteGuard &other);
public:
	RWLockWriteGuard(RWLock *lock);
	~RWLockWriteGuard();
	void Unlock();
	void Lock();
protected:
	RWLock *lock;
	bool locked;
};

} // namespace l2server

