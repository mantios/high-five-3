
#include <Server/BuilderCmd2Handler.h>
#include <Server/User.h>
#include <Server/CUserSocket.h>
#include <Server/CObjectDB.h>
#include <Server/Server.h>
#include <Server/CDB.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/Config.h>
#include <Common/CSharedCreatureData.h>
#include <sstream>

namespace l2server {

void BuilderCmd2Handler::Init()
{
	WriteInstructionCall(0x9F26DC, reinterpret_cast<UINT32>(Handler));
}

bool __cdecl BuilderCmd2Handler::Handler(CUserSocket *socket, const BYTE *packet)
{
	GUARDED;

	// Get current user
	User *user = socket->user;
	if (!user) {
		return false;
	}

	// Get command into wstring and check it's length
	std::wstring buffer(reinterpret_cast<const wchar_t*>(packet));
	if (buffer.size() > 900) {
		return false;
	}

	// Get target object
	CSPointer<CObject> targetObject = user->GetTarget();

	// Cast target object (if any) to target creature
	CCreature *target = (targetObject && targetObject->IsCreature()) ? targetObject->CastCreature() : 0;

	// Log builder command to system log
	if (target) {
		CLog::Add(CLog::Blue, L"[AUDIT] BuilderCmd %s -> %s: %s", user->GetName(), target->GetName(), buffer.c_str());
	} else {
		CLog::Add(CLog::Blue, L"[AUDIT] BuilderCmd %s -> null: %s", user->GetName(), buffer.c_str());
	}

	// Custom builder commands + my favourite aliases/abbreviations
	if (buffer == L"gmliston") {
		return reinterpret_cast<bool(*)(CUserSocket*, User*, const wchar_t*)>(0x4B7264)(socket, user, L"//gmreg");
	} else if (buffer == L"gmlistoff") {
		return reinterpret_cast<bool(*)(CUserSocket*, User*, const wchar_t*)>(0x4B74C0)(socket, user, L"//gmunreg");
	} else if (buffer == L"res") {
		return reinterpret_cast<bool(*)(CUserSocket*, User*, const wchar_t*)>(0x4C4604)(socket, user, L"//resurrect 100");
	} else if (buffer == L"res 0") {
		return reinterpret_cast<bool(*)(CUserSocket*, User*, const wchar_t*)>(0x4C4604)(socket, user, L"//resurrect 0");
	} else if (buffer == L"heal") {
		if (target) {
			return reinterpret_cast<bool(*)(CUserSocket*, User*, const wchar_t*)>(0x50235C)(socket, user, target->GetName());
		} else {
			return reinterpret_cast<bool(*)(CUserSocket*, User*, const wchar_t*)>(0x50235C)(socket, user, user->GetName());
		}
	} else if (buffer.substr(0, 4) == L"say ") {
		return Say(socket, user, target, packet, buffer);
	} else if (buffer.substr(0, 14) == L"drop_item_each") {
		return DropItemEach(socket, user, target, packet, buffer);
	} else if (buffer.substr(0, 5) == L"give ") {
		return Give(socket, user, target, packet, buffer);
	} else if (buffer.substr(0, 5) == L"take ") {
		return Take(socket, user, target, packet, buffer);
	} else if (buffer.substr(0, 5) == L"diag ") {
		return Diag(socket, user, target, packet, buffer);
	} else if (buffer == L"debug 0") {
		Server::SetDebug(false);
		return false;
	} else if (buffer == L"debug 1") {
		Server::SetDebug(true);
		return false;
	} else if (buffer == L"test") {
		// Various test stuff can be put here like this:
		//CDB::Instance()->RequestTest(100);
		return false;
	} else {
		// Call original handler
		return reinterpret_cast<bool(*)(CUserSocket*, const BYTE*)>(0x510464)(socket, packet);
	}
}

bool BuilderCmd2Handler::Say(CUserSocket *socket, User *user, CCreature *target, const BYTE *packet, const std::wstring &buffer)
{
	GUARDED;

	// Critical announce with builder's name
	std::wstring sentence = user->GetName();
	sentence += L": ";
	sentence += buffer.substr(4);

	// Check if it doesn't exceed 900 chars, if so, shorten it and add three dots
	if (sentence.size() > 900) {
		sentence = sentence.substr(0, 897) + L"...";
	}

	// Call BuilderCmd_critannounce
	return reinterpret_cast<bool(*)(CUserSocket*, User*, const wchar_t*)>(0x4A53A4)(socket, user, sentence.c_str());
}

bool BuilderCmd2Handler::DropItemEach(CUserSocket *socket, User *user, CCreature *target, const BYTE *packet, const std::wstring &buffer)
{
	GUARDED;

	// This is usually favourite command to crash the server
	// When user makes more than 1000 items pop up at once, it may cause problems so we'll check the amount and warn the user

	// Get item type and count from buffer via stringstream
	std::wstringstream s;
	s << buffer;
	std::wstring cmd;
	std::wstring itemType;
	UINT64 count = 0;
	s >> cmd >> itemType >> count;

	// Check count
	if (count > 1000) {
		// Send warning message
		user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"That's not a good idea...");
		return false;
	} else {
		// Call original handler
		return reinterpret_cast<bool(*)(CUserSocket*, const BYTE*)>(0x510464)(socket, packet);
	}
}

bool BuilderCmd2Handler::Give(CUserSocket *socket, User *user, CCreature *target, const BYTE *packet, const std::wstring &buffer)
{
	GUARDED;

	// Check if target is user
	if (!target || !target->IsUser()) {
		user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Invalid target");
		return false;
	}

	// Get item type and count from buffer via stringstream
	std::wstringstream s;
	s << buffer;
	std::wstring cmd;
	std::wstring itemType;
	UINT64 count = 0;
	s >> cmd >> itemType >> count;

	// Check if count is sane
	if (count <= 0) {
		user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Invalid count");
		return false;
	}

	// Find item by class
	int itemTypeId = CObjectDB::Instance()->GetClassIdFromName(itemType.c_str());

	// Check result
	if (itemTypeId <= 0) {
		// Maybe it's already ID?
		itemTypeId = _wtoi(itemType.c_str());
		if (itemTypeId <= 0) {
			// Send error message
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Unknown item");
			return false;
		}
	}

	// Try to add item(s) to inventory
	if (target->AddItemToInventory(itemTypeId, count)) {
		// Send acknowledgement message
		user->SendSystemMessageFmt(Config::Instance()->server->name.c_str(),
			L"Gave %dx item %s (%d) to %s",
			count, itemType.c_str(), itemTypeId, target->GetName());
	} else {
		// Send error message
		user->SendSystemMessageFmt(Config::Instance()->server->name.c_str(),
			L"Failed to give %dx item %s (%d) to %s",
			count, itemType.c_str(), itemTypeId, target->GetName());
	}
	return false;
}

bool BuilderCmd2Handler::Take(CUserSocket *socket, User *user, CCreature *target, const BYTE *packet, const std::wstring &buffer)
{
	GUARDED;

	// Check that target is user
	if (!target || !target->IsUser()) {
		user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Invalid target");
		return false;
	}

	// Get item type and count from buffer via stringstream
	std::wstringstream s;
	s << buffer;
	std::wstring cmd;
	std::wstring itemType;
	UINT64 count = 0;
	s >> cmd >> itemType >> count;

	// Check if count is sane
	if (count <= 0) {
		user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Invalid count");
		return false;
	}

	// Find item by class
	int itemTypeId = CObjectDB::Instance()->GetClassIdFromName(itemType.c_str());

	// Check result
	if (itemTypeId <= 0) {
		// Maybe it's already ID?
		itemTypeId = _wtoi(itemType.c_str());
		if (itemTypeId <= 0) {
			// Send error message
			user->SendSystemMessage(Config::Instance()->server->name.c_str(), L"Unknown item");
			return false;
		}
	}

	// Try to remove item(s) from inventory
	if (target->DeleteItemInInventory(itemTypeId, count)) {
		// Send acknowledgement message
		user->SendSystemMessageFmt(Config::Instance()->server->name.c_str(),
			L"Taken %dx item %s (%d) from %s",
			count, itemType.c_str(), itemTypeId, target->GetName());
	} else {
		// Send error message
		user->SendSystemMessageFmt(Config::Instance()->server->name.c_str(),
			L"Failed to take %dx item %s (%d) from %s",
			count, itemType.c_str(), itemTypeId, target->GetName());
	}
	return false;
}

bool BuilderCmd2Handler::Diag(CUserSocket *socket, User *user, CCreature *target, const BYTE *packet, const std::wstring &buffer)
{
	GUARDED;

	if (!target) {
		user->SendSystemMessageFmt(Config::Instance()->server->name.c_str(),
			L"No target selected");
		return false;
	}
	if (!target->IsUser()) {
		user->SendSystemMessageFmt(Config::Instance()->server->name.c_str(),
			L"Target not user");
		return false;
	}
	CUserSocket *targetSocket = reinterpret_cast<User*>(target)->GetUserSocket();
	if (!targetSocket) {
		user->SendSystemMessageFmt(Config::Instance()->server->name.c_str(),
			L"Target has no socket");
		return false;
	}
	std::wstringstream s;
	s << buffer;
	std::wstring cmd;
	int diagType;
	s >> cmd >> diagType;
	if (diagType < 1 || diagType > 255) {
		user->SendSystemMessageFmt(Config::Instance()->server->name.c_str(),
			L"Invalid diagtype");
		return false;
	}
	targetSocket->Send("cdddddddddhddd", 0x48, target->objectId, target->objectId,
		/*32767*/ 1204, diagType, 100, 100, target->sd->location.x, target->sd->location.y, target->sd->location.z, 0,
		target->sd->location.x, target->sd->location.y, target->sd->location.z);
	return false;
}

} // namespace l2server

