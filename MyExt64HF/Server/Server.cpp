
#include <Server/Server.h>
#include <Server/ShutdownChecker.h>
#include <Server/CUserSocket.h>
#include <Server/CUserSocketReleaseVerifier.h>
#include <Server/CHTMLCacheManager.h>
#include <Server/User.h>
#include <Server/BuilderCmd.h>
#include <Server/CDB.h>
#include <Server/NpcServer.h>
#include <Server/CInventory.h>
#include <Server/CCreature.h>
#include <Server/CWareHouseDeposit.h>
#include <Server/CMultiPartyCommandChannelManager.h>
#include <Server/CSummon.h>
#include <Server/COlympiadSystem.h>
#include <Server/COlympiadDefaultSetting.h>
#include <Server/CSkillInfo.h>
#include <Server/CEventManager.h>
#include <Server/CContributeData.h>
#include <Server/CWorld.h>
#include <Server/CItem.h>
#include <Server/CPet.h>
#include <Server/CUserBroadcaster.h>
#include <Server/CNPC.h>
#include <Server/CNewAction.h>
#include <Server/EnchantItem.h>
#include <Common/Config.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <stdio.h>

namespace l2server {

bool Server::debug = false;
Server::Plugin *Server::plugin = 0;

void Server::Init()
{
	// Set debug mode
	SetDebug(Config::Instance()->server->debug);

	// Set deadlock timeout
	DeadlockTimeout(Config::Instance()->server->deadlockTimeout * 1000 * 1000);

	// Disable exit when authd can't be reached
	DisableNoAuthExit();

	// Disable sending mail to NCsoft
	DisableSendMail();

	// Set shutdown duration
	SetShutdownSeconds(Config::Instance()->server->shutdownDuration);

	// Hook start and load
	HookStart();
	HookLoad();

	// Load plugin
	if (!Config::Instance()->server->plugin.empty()) {
		if (HMODULE mod = LoadLibrary(Config::Instance()->server->plugin.c_str())) {
			FARPROC proc = GetProcAddress(mod, "factory");
			if (proc) {
				plugin = reinterpret_cast<Plugin*(*)()>(proc)();
			}
			else {
				MessageBox(0, L"Can't load function factory from plugin", L"Error", 0);
				exit(1);
			}
		}
		else {
			MessageBox(0, L"Can't load plugin", L"Error", 0);
			exit(1);
		}
	}

	// Disable setting thread core affinity
	DontSetThreadCoreAffinity();

	// Hard-coded soulshot and spiritshot IDs
	WriteMemoryDWORD(0xFEF1C0, 0x7D56); // our shadow soulshot B-grade
	WriteMemoryDWORD(0xFEF4C8, 0x7D57); // our shadow blessed spiritshot B-grade

	// Hook TimerExpired
	WriteMemoryQWORD(0xC8D5E8, FnPtr(Server::TimerExpired));

	// Initialize UTF-8 support
	InitUtf8Support();

	// Disable cooldown reset on skill acquire
	DontResetCooldownOnSkillAcquire();

	// Keep songs/dances across relog
	if (Config::Instance()->fixes->relogKeepSongsDances) RelogKeepSongsDances();

	// Initialize shutdown checker
	ShutdownChecker::Init();

	// Initialize CUserSocket
	CUserSocket::Init();

	// Initialize CUserSocketReleaseVerifier
	CUserSocketReleaseVerifier::Init();

	// Initialize CHTMLCacheManager
	CHTMLCacheManager::Init();

	// Initialize User
	User::Init();

	// Initialize CDB
	CDB::Init();

	// Initialize custom builder commands
	BuilderCmd_Init();

	// Initialize npc packet ext
	NpcServer::Init();

	// Initialize inventory ext
	CInventory::Init();

	// Initialize creature ext
	CCreature::Init();

	// Initialize warehouse deposit ext
	CWareHouseDeposit::Init();

	// Initialize multiparty command channel manager ext
	CMultiPartyCommandChannelManager::Init();

	// Initialize summon/pet ext
	CSummon::Init();

	// Initialize olympiad system ext
	COlympiadSystem::Init();

	// Initialize olympiad default setting ext
	COlympiadDefaultSetting::Init();

	// Initialize skill info ext
	CSkillInfo::Init();

	// Initialize event manager
	CEventManager::Init();

	// Initialize contribute data
	CContributeData::Init();

	// Initialize world ext
	CWorld::Init();

	// Initialize item ext
	CItem::Init();

	// Initialize pet ext
	CPet::Init();

	// Initialize user broadcaster
	CUserBroadcaster::Init();

	// Initialize NPC ext
	CNPC::Init();

	// Initialize clan restrictions
	InitClanRestrictions();

	// Initialize attack action
	CAttackAction::Init();

	// Initialize enchant item
	EnchantItem::Init();

	// Hide numerous warnings
	HideWarnings(); // call this last!
}

void Server::Load()
{
	GUARDED;

	EnchantItem::Load();
}

bool Server::IsDebug()
{
	return debug;
}

void Server::SetDebug(bool debug)
{
	Server::debug = debug;
}

void Server::DeadlockTimeout(UINT32 timeout)
{
	WriteMemoryDWORD(0x705F20, timeout);
}

void Server::DisableNoAuthExit()
{
	WriteInstructionJmp(0x73007A, 0x72FF54);
}

void Server::DisableSendMail()
{
	NOPMemory(0x66E730 + 0x21, 5);
	NOPMemory(0x66EA78 + 0x2A4, 5);
	NOPMemory(0x670110 + 0x106, 5);
	NOPMemory(0x67025C + 0x140, 5);
	NOPMemory(0x6703C0 + 0xFE, 5);
}

void Server::HideWarnings()
{
	NOPMemory(0x74E5D7, 5);
	NOPMemory(0x9EF5AE, 5);
	NOPMemory(0x734BBC, 5);
	NOPMemory(0x83AE2D, 5);
	NOPMemory(0x87CE63, 5);
	NOPMemory(0x87D06C, 5);
	//NOPMemory(0x8BBF09, 5); // FloatERR [%s] minus HP input...
	//NOPMemory(0x8BBF12, 5); // + call stack
	NOPMemory(0x734F1E, 5);
}

void Server::SetShutdownSeconds(const int seconds)
{
	WriteMemoryDWORD(0x706EC3 + 3, static_cast<UINT32>(seconds));
}

void Server::HookStart()
{
	WriteInstructionCall(0x730DEF, reinterpret_cast<UINT32>(StartHook));
	WriteInstructionCall(0x72311B, reinterpret_cast<UINT32>(CreateWindowEx), 0x723121);
}

HWND Server::CreateWindowEx(DWORD dwExStyle, LPCWSTR lpClassName, LPCWSTR lpWindowName, DWORD dwStyle, int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam)
{
	// Change window title
	std::wstring name(lpWindowName);
	name += L" - patched by MyExt64HF";

	// Call original CreateWindowEx
	return ::CreateWindowEx(dwExStyle, lpClassName, name.c_str(), dwStyle, X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
}

void Server::StartHook(void *logger, int level, const wchar_t *fmt, const wchar_t *build)
{
	// Call original logging
	reinterpret_cast<void(*)(void*, int, const wchar_t*, const wchar_t*)>(0x917E58)(logger, level, fmt, build);

	// Move old dumps to backup directory
	ShellExecute(0, L"open", L"cmd.exe", L"/C mkdir bak", 0, SW_HIDE);
	ShellExecute(0, L"open", L"cmd.exe", L"/C move LinError.txt.*.bak bak\\", 0, SW_HIDE);
	ShellExecute(0, L"open", L"cmd.exe", L"/C move minidump_*.dmp bak\\", 0, SW_HIDE);
}

void Server::HookLoad()
{
	WriteInstructionCall(0x72D6DE, reinterpret_cast<UINT32>(LoadHook));
}

void Server::LoadHook()
{
	// Call original function
	reinterpret_cast<void(*)()>(0x716EDC)();

	// Call our Load()
	Load();
}

Server::Plugin* Server::GetPlugin()
{
	return plugin;
}

void Server::TimerExpired(void *obj, int id)
{
	// start ShutdownChecker and our own timer (id = 100, interval = 1000 ms)
	static bool shutdownCheckerStarted = false;
	if (!shutdownCheckerStarted) {
		CLog::Add(CLog::Blue, L"Patched by MyExt64HF");
		ShutdownChecker::Start();
		shutdownCheckerStarted = true;
		// Schedule our own timer (id = 100, interval = 1000 ms)
		reinterpret_cast<void(*)(void*, int, int)>(0x7048C4)(obj, 1000, 100);
	}
	if (id != 100) {
		// Call original timer
		reinterpret_cast<void(*)(void*, int)>(0x731E80)(obj, id);
		return;
	}
	// Schedule our own timer again (id = 100, interval = 1000 ms)
	reinterpret_cast<void(*)(void*, int, int)>(0x7048C4)(obj, 1000, 100);

	// Check for shutdown
	ShutdownChecker::CheckForShutdown();

	// We may add other stuff here that should be called every second
}

void Server::DontSetThreadCoreAffinity()
{
	NOPMemory(0x705C4B, 6); // let OS decide which thread will execute on which core
}

void Server::RelogKeepSongsDances()
{
	NOPMemory(0x5A5261, 2); // skip check for is_magic == 3
}

void Server::InitUtf8Support()
{
	// support UTF-8 scripts
	WriteInstructionCall(0x6D67CC + 0xBA, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0xA251A8 + 0x2E9, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0xA231D5, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x71B183, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x87AABE, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x87AB03, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x87B0A1, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0xA241E6, FnPtr(ReadNextWchar));
	WriteInstructionCall(0xA24200, FnPtr(ReadNextWchar));
	WriteInstructionCall(0xA2422C, FnPtr(ReadNextWchar));
	WriteInstructionCall(0xA33B7E, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0xA33C06, FnPtr(ReadNextWchar));

	WriteMemoryBYTES(0x6D84A8, "\x48\x8D\x8E\xD6\x02\x00\x00", 7); // lea rcx, [rsi+0x2D6]
	WriteMemoryBYTES(0x6D84AF, "\x48\x8D\x96\xAC\x02\x00\x00", 7); // lea rdx, [rsi+0x2ac]
	WriteInstructionCall(0x6D84B6, FnPtr(DecodeAndCopyHTML));
	WriteMemoryBYTES(0x6D84BB, "\x48\x89\xC7", 3); // mov rdi, rax
	NOPMemory(0x6D84BE, 15);
}

void Server::DontResetCooldownOnSkillAcquire()
{
	// Just jump over
	WriteInstructionJmp(0x58C88C, 0x58C8AB);
}

void Server::InitClanRestrictions()
{
	WriteMemoryDWORD(0xFFFA48, FnPtr(Config::Instance()->clanRestrictions->pledgeWarTimeout));
	WriteMemoryDWORD(0xFFFA4C, FnPtr(Config::Instance()->clanRestrictions->pledgeOustPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA50, FnPtr(Config::Instance()->clanRestrictions->pledgeWithdrawPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA54, FnPtr(Config::Instance()->clanRestrictions->pledgeOustedPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA58, FnPtr(Config::Instance()->clanRestrictions->pledgeRechallengePenaltyTimeout));
	WriteMemoryDWORD(0xFFFA5C, FnPtr(Config::Instance()->clanRestrictions->pledgeRechallengeDespiteRejectionPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA60, FnPtr(Config::Instance()->clanRestrictions->pledgeDismissTimeout));
	WriteMemoryDWORD(0xFFFA64, FnPtr(Config::Instance()->clanRestrictions->pledgeDismissPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA68, FnPtr(Config::Instance()->clanRestrictions->pledgeDismissByNPC));
	WriteMemoryDWORD(0xFFFA60, FnPtr(Config::Instance()->clanRestrictions->allianceCanAcceptNewMemberPledge));
	WriteMemoryDWORD(0xFFFA6C, FnPtr(Config::Instance()->clanRestrictions->allianceOustPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA70, FnPtr(Config::Instance()->clanRestrictions->allianceWithdrawPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA74, FnPtr(Config::Instance()->clanRestrictions->allianceOustedPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA78, FnPtr(Config::Instance()->clanRestrictions->allianceDismissPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA7C, FnPtr(Config::Instance()->clanRestrictions->allianceRechallengePenaltyTimeout));
	WriteMemoryDWORD(0xFFFA80, FnPtr(Config::Instance()->clanRestrictions->allianceRechallengeDespiteRejectionPenaltyTimeout));
	WriteMemoryDWORD(0xFFFA84, FnPtr(Config::Instance()->clanRestrictions->castleAnnounceTime));
	WriteMemoryDWORD(0xFFFA88, FnPtr(Config::Instance()->clanRestrictions->castleStandbyTime));
}

} // namespace server

