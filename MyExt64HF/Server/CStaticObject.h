
#pragma once

#include <Server/CWorldObject.h>

namespace l2server {

class CStaticObject : public CWorldObject {
public:
	static const UINT64 vtable = 0xD6F328;
};

} // namespace l2server

