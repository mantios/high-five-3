
#include <Server/CSkillInfo.h>
#include <Server/CItem.h>
#include <Server/CCreature.h>
#include <Common/CSharedCreatureData.h>
#include <Common/Utils.h>
#include <Common/Config.h>

namespace l2server {

void CSkillInfo::Init()
{
	WriteMemoryBYTES(0x8E15B1,
		/* 0x0000 */ "\x8B\x4D\x14"         // mov ecx, dword ptr [rbp + 0x14]
		/* 0x0003 */ "\x48\x89\xFA"         // mov rdx, rdi
		/* 0x0006 */ "\x00\x00\x00\x00\x00" // placeholder for call
		/* 0x000B */ "\x84\xC0",            // test al, al
		/* 0x000D */ 0xD);
	WriteInstructionCall(0x8E15B1 + 6, FnPtr(CanEnchantAttribute));
	NOPMemory(0x8E15BE, 7);
	WriteMemoryBYTE(0x8E15C5, 0x75); // jle -> jnz

	NOPMemory(0x974275, 0x1C);
	WriteMemoryBYTES(0x974275,
		/* 0x0000 */ "\x4C\x89\xF9"  // mov rcx, r15
		/* 0x0003 */ "\x4C\x89\xF2", // mov rdx, r14
		/* 0x0006 */ 6);
	WriteInstructionCall(0x974275 + 6, FnPtr(&CSkillInfo::GetConflictingAttributes));

	static double skillLandProbMin(Config::Instance()->custom->skillLandProbMin);
	static double skillLandProbMax(Config::Instance()->custom->skillLandProbMax);
	WriteAddress(0x8CB01F + 4, FnPtr(&skillLandProbMin));
	WriteAddress(0x8CB047 + 4, FnPtr(&skillLandProbMin));
	WriteAddress(0x8CCF6A + 4, FnPtr(&skillLandProbMin));
	WriteAddress(0x8CB033 + 4, FnPtr(&skillLandProbMax));
	WriteAddress(0x8CB05B + 4, FnPtr(&skillLandProbMax));
	WriteAddress(0x8CCF7E + 4, FnPtr(&skillLandProbMax));

	WriteInstructionCall(0x5825E0 + 0x1BA, FnPtr(&CSkillInfo::IsValidTarget));
	WriteInstructionCall(0x5AE0AC + 0xA9, FnPtr(&CSkillInfo::IsValidTarget));
	WriteInstructionCall(0x5AECA0 + 0xCF, FnPtr(&CSkillInfo::IsValidTarget));
	WriteInstructionCall(0x78C8D4 + 0x88, FnPtr(&CSkillInfo::IsValidTarget));
	WriteInstructionCall(0x78ED20 + 0x495, FnPtr(&CSkillInfo::IsValidTarget));
}

bool CSkillInfo::CanEnchantAttribute(AttributeTypeEnum type, CItem *item)
{
	if (type >= AttributeTypeMax) return false;
	if (Config::Instance()->custom->allowMultipleAttributesOnItem) {
		int oppositeType = static_cast<int>(type) ^ 1;
		return reinterpret_cast<const INT16*>(&item->baseAttribute)[2 + oppositeType] == 0;
	}
	for (size_t i = 0 ; i < 6 ; ++i) {
		if (i == static_cast<int>(type)) continue;
		if (reinterpret_cast<const INT16*>(&item->baseAttribute)[2 + i]) return false;
	}
	return true;
}

void CSkillInfo::GetConflictingAttributes(bool *conflictingAttributes)
{
	if (Config::Instance()->custom->allowMultipleAttributesOnItem) {
		for (size_t i = 0 ; i < 6 ; ++i) {
			conflictingAttributes[i] = enchantAttributeTypes[i];
		}
	} else {
		for (size_t i = 0 ; i < 6 ; ++i) {
			conflictingAttributes[i] = !enchantAttributeTypes[i ^ 1];
		}
	}
}

bool CSkillInfo::IsValidTarget(CCreature *attacker, CCreature *target, bool b) const
{
	GUARDED;

	if (target && target->IsBoss() && target->objectType == 1029028) { // valakas
		if (abs(attacker->sd->location.z - target->sd->location.z) > 128) {
			return false;
		}
	}

	return reinterpret_cast<bool(*)(const CSkillInfo*, CCreature*, CCreature*, bool)>(0x8C9CC0)(this, attacker, target, b);
}

} // namespace server

