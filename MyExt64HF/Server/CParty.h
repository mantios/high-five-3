
#pragma once

#include <Common/xstd.h>
#include <Common/Utils.h>

namespace l2server {

class User;
class CMultiPartyCommandChannel;

class CParty {
public:
	void SendRelationUpdates();
	xstd::vector<User*> GetAllMember();
	xstd::vector<User*> GetAllMemberInRange(const FVector &location, const double distance);
	CMultiPartyCommandChannel* GetMPCC();
	User* GetMaster();
	void Dismiss(bool b);
	void Withdraw(User *user, bool b);
};

} // namespace l2server

