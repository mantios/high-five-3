
#pragma once

#include <Server/CObject.h>
#include <Common/CSPointer.h>

namespace l2server {

class CInventory {
public:
	static void Init();

	static void GetObjectTradeFix(void*, CSPointer<CObject> &ptr, UINT32 index);

	bool CheckAddable(int itemType, INT64 amount, bool unknown = false);

	unsigned char padding0x0000[0x00D0 - 0x0000];
};

} // namespace l2server

