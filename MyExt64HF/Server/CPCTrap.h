
#pragma once

#include <Server/CTrap.h>

namespace l2server {

class CPCTrap : public CTrap {
public:
	static const UINT64 vtable = 0xD7C368;
};

} // namespace l2server

