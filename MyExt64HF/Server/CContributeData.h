
#pragma once

#include <Server/CObject.h>
#include <Common/xstd.h>

namespace l2server {

class CContributeData : public CObject {
public:
	static void Init();
	void Clear();
	bool PickableIn5Sec(CCreature *creature);

	/* 0x0040 */ unsigned char padding0x0040[0x0060 - 0x0040];
	/* 0x0060 */ xstd::map<int, double> data;
	/* 0x0078 */ int looterId;
	/* 0x007C */ unsigned char padding0x007C[0x0098 - 0x007C];
	/* 0x0098 */ int topContributorId;
	/* 0x009C */ int topContributorLevel;
	/* 0x00A0 */ unsigned int topDamage;
};

} // namespace l2server

