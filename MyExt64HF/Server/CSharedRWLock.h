
#pragma once

#include <windows.h>

namespace l2server {

// CSharedRWLock used on many places
class CSharedRWLock {
public:
	void ReadLock(bool concession);
	void WriteLock();
	void Done();
};

// Scope guard (read lock) for CSharedRWLock
class CSharedRWLockReadGuard {
private:
	CSharedRWLockReadGuard();
	CSharedRWLockReadGuard(const CSharedRWLockReadGuard &other);
public:
	CSharedRWLockReadGuard(CSharedRWLock *lock);
	~CSharedRWLockReadGuard();
	void Unlock();
	void Lock();
protected:
	CSharedRWLock *lock;
	bool locked;
};

// Scope guard (write lock) for CSharedRWLock
class CSharedRWLockWriteGuard {
private:
	CSharedRWLockWriteGuard();
	CSharedRWLockWriteGuard(const CSharedRWLockWriteGuard &other);
public:
	CSharedRWLockWriteGuard(CSharedRWLock *lock);
	~CSharedRWLockWriteGuard();
	void Unlock();
	void Lock();
protected:
	CSharedRWLock *lock;
	bool locked;
};

} // namespace l2server

