
#pragma once

#include <Server/CNPC.h>

namespace l2server {

class CTeleporter : public CNPC {
public:
	static const UINT64 vtable = 0xD729A8;
};

} // namespace l2server

