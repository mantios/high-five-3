
#pragma once

#include <Windows.h>

namespace l2server {

class CNewAction {
public:
	static const UINT64 vtable = 0xB415C8;

	/* 0x0000 */ unsigned char padding0x0000[0x0008 - 0x0000];
	/* 0x0008 */ int pending;
	/* 0x000C */ unsigned char padding0x000C[0x0018 - 0x000C];
	/* 0x0018 */ UINT32 actorId;
	/* 0x001C */ unsigned char padding0x001C[0x0020 - 0x001C];
};

class CNothingAction : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF368;
};

class CStandAction : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF3D8;
};

class CSitAction : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF448;
};

class CAttackAction : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF4B8;

	/* 0x0020 */ UINT32 targetId;

	static void Init();
	static bool CheckAttackValidity(class CCreature *attacker, class CCreature *target);
};

class CTalkAction : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF528;
};

class CGeneralAction : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF598;
};

class CGetItemAction2 : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF608;
};

class COpenCloseAction2 : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF678;
};

class CAttackWaitAction : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF6E8;
};

class CPrivateStoreAction : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF758;

	enum Type {
		TypeSell = 1,
		TypeBuy = 2,
		TypeSellSet = 3,
		TypeBuySet = 4,
		TypeManufacture = 5,
		TypeManufactureSet = 6,
		TypePackageSell = 7,
		TypePackageSellSet = 8
	};

	/* 0x0020 */ Type type;
};

class CCoupleAction : public CNewAction {
public:
	static const UINT64 vtable = 0xBDF7C8;
};

class CMoveAction : public CNewAction {
public:
	static const UINT64 vtable = 0xCAA098;
};

class CSkillAction2 : public CNewAction {
public:
	static const UINT64 vtable = 0xCAE698;

	/* 0x0000 */ unsigned char padding0x0000[0x0018-0x0000];
	/* 0x0018 */ UINT32 attackerId;
	/* 0x001C */ unsigned char padding0x000C[0x0020-0x001C];
	/* 0x0020 */ UINT32 targetId;
};

class CMoveActionBase : public CNewAction {
public:
	static const UINT64 vtable = 0xCA9E18;
};

class CMoveToTargetAction : public CMoveActionBase {
public:
	static const UINT64 vtable = 0xBDFC48;
};

class CMoveToTargetRelativeAction : public CMoveActionBase {
public:
	static const UINT64 vtable = 0xBDFCB8;
};

class CMoveToTargetWaitAction : public CMoveActionBase {
public:
	static const UINT64 vtable = 0xBDFD28;
};

class CMoveRelativeAction : public CMoveActionBase {
public:
	static const UINT64 vtable = 0xBDFD98;
};

} // namespace l2server

