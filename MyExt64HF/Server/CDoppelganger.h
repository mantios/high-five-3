
#pragma once

#include <Server/CNPC.h>

namespace l2server {

class CDoppelganger : public CNPC {
public:
	static const UINT64 vtable = 0xC23778;
};

} // namespace l2server

