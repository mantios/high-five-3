
#pragma once

#include <Server/CStaticObject.h>

namespace l2server {

class CTeamBattleFlag : public CStaticObject {
public:
	static const UINT64 vtable = 0xD72168;
};

} // namespace l2server

