
#pragma once

#include <windows.h>

namespace l2server {

class EnchantItem {
public:
	static void Init();
	static void Load();

protected:
	static double __cdecl GetFighterWeaponChance(const int level, class CItem *item);
	static double __cdecl GetMageWeaponChance(const int level, class CItem *item);
	static double __cdecl GetArmorChance(const int level, const bool onePiece);
	static double __cdecl GetEventWeaponChance(const int level);
};

} // namespace l2server

