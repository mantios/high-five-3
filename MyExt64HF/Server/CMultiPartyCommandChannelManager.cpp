
#include <Server/CMultiPartyCommandChannelManager.h>
#include <Server/CMultiPartyCommandChannel.h>
#include <Server/CParty.h>

namespace l2server {

void CMultiPartyCommandChannelManager::Init()
{
	// Send relation updates on join MPCC
	WriteInstructionCall(0x445FE0 + 0x1A0, FnPtr(&CMultiPartyCommandChannelManager::Join));
	WriteInstructionCall(0x9E1C04 + 0x373, FnPtr(&CMultiPartyCommandChannelManager::Join));
	WriteInstructionCall(0x9E1C04 + 0x40F, FnPtr(&CMultiPartyCommandChannelManager::Join));

	// Send relation updates on oust from MPCC
	WriteInstructionCall(0x9E95A9, FnPtr(&CMultiPartyCommandChannelManager::Oust));

	// Send relation updates on withdraw
	WriteInstructionCall(0x76EE08 + 0x76, FnPtr(&CMultiPartyCommandChannelManager::WithdrawInternal));
	WriteInstructionCall(0x76FD34 + 0x1B0, FnPtr(&CMultiPartyCommandChannelManager::WithdrawInternal));
}

void CMultiPartyCommandChannelManager::Join(CSPointer<User> &owner_, CSPointer<User> &target_)
{
	CSPointerHelper<User> owner(owner_);
	CSPointerHelper<User> target(target_);
	reinterpret_cast<void(*)(CMultiPartyCommandChannelManager*, CSPointer<User>&, CSPointer<User>&)>(0x76C5E0)(
		this, owner.GetCopy(), target.GetCopy());
	if (!target) return;
	CMultiPartyCommandChannel *mpcc = target->GetMPCC();
	if (!mpcc) return;
	mpcc->SendRelationUpdates();
}

void CMultiPartyCommandChannelManager::Oust(CSPointer<User> &owner_, CSPointer<User> &target_)
{
	CSPointerHelper<User> owner(owner_);
	CSPointerHelper<User> target(target_);
	CMultiPartyCommandChannel *mpcc = 0;
	CParty *party = 0;
	if (target) {
		mpcc = target->GetMPCC();
		party = target->GetParty();
	}
	reinterpret_cast<void(*)(CMultiPartyCommandChannelManager*, CSPointer<User>&, CSPointer<User>&)>(0x76D09C)(
		this, owner.GetCopy(), target.GetCopy());
	if (mpcc) mpcc->SendRelationUpdates();
	if (party) party->SendRelationUpdates();
}

void CMultiPartyCommandChannelManager::WithdrawInternal(CParty *party)
{
	CMultiPartyCommandChannel *mpcc = 0;
	if (party) mpcc = party->GetMPCC();
	reinterpret_cast<void(*)(CMultiPartyCommandChannelManager*, CParty*)>(0x76DC38)(this, party);
	if (mpcc) mpcc->SendRelationUpdates();
	if (party) party->SendRelationUpdates();
}

} // namespace l2server

