
#include <Server/CUserSocketReleaseVerifier.h>
#include <Server/User.h>
#include <Server/CUserSocket.h>
#include <Common/Utils.h>
#include <Common/CSharedCreatureData.h>

namespace l2server {

void CUserSocketReleaseVerifier::Init()
{
	// Offline trade
	WriteMemoryBYTES(0xA1279F, "\x48\x89\xF1", 3); // mov rcx, rsi (user ptr)
	WriteInstructionCallJmpEax(0xA127A2, FnPtr(OfflineTradeCheckHelper));
	WriteInstructionCall(0xA127CD, FnPtr(&CUserSocketReleaseVerifier::OfflineTradeTimerHelper));
}

UINT64 CUserSocketReleaseVerifier::OfflineTradeCheckHelper(User *user)
{
	if (user->sd->inCombat) return 0xA127AF;
	if (user->ext.offlineTrade) return 0xA127AF;
	return 0xA127E5;
}

void CUserSocketReleaseVerifier::OfflineTradeTimerHelper(const int interval, const int id)
{
	GUARDED;

	if (socket) {
		if (User *user = socket->user) {
			ScopedLock lock(user->ext.cs);
			if (user->ext.offlineTrade) {
				user->ext.offlineTradeUserSocketReleaseVerifier = this;
				return;
			}
		}
	}

	AddTimer(interval, id);
}

static void check()
{
	static_assert(offsetof(CUserSocketReleaseVerifier, socket) == 0x18);
}

} // namespace l2server

