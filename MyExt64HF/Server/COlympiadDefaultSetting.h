
#pragma once

#include <Windows.h>

namespace l2server {

class COlympiadDefaultSetting {
public:
	static const double penaltyPointDivider;

	static UINT32 &olympiadMinimumCountTeam;
	static UINT32 &olympiadMinimumCountNonclass;
	static UINT32 &olympiadMinimumCountClass;

	static void Init();

	COlympiadDefaultSetting* Constructor();
};

} // l2server

