
#pragma once

#include <Server/CCreature.h>
#include <Common/xstd.h>

namespace l2server {

class CObjectDB;
class User;
class GoodStruct;
enum ObjectFieldType;

class ItemDropStruct {
public:
	int itemType;
	int amountMin;
	int amountMax;
	double chance;
};

class ItemDropMultiStruct {
public:
	xstd::vector<ItemDropStruct*> *items;
	double chance;
};

class CNPC : public CCreature {
public:
	static const UINT64 vtable = 0xCB0598;

	/* 0x0B40 */ virtual void vfn0x0B40() {}
	/* 0x0B48 */ virtual bool IsEnemyToAsNpc_0x0B48(CCreature *creature) { return false; }
	/*        */ inline bool IsEnemyToAsNpc(CCreature *creature) { return IsEnemyToAsNpc_0x0B48(creature); }
	/* 0x0B50 */ virtual void EnterWorld_0x0B50(bool, int, int, unsigned int) {}
	/*        */ inline void EnterWorld(bool b, int i, int j, unsigned int k) { EnterWorld_0x0B50(b, i, j, k); }
	/* 0x0B58 */ virtual void OnTeleport_0x0B58() {}
	/*        */ inline void OnTeleport() { OnTeleport_0x0B58(); }
	/* 0x0B60 */ virtual void SendNPCInfo_0x0B60(User*, bool) {}
	/*        */ inline void SendNPCInfo(User *user, bool b) { SendNPCInfo_0x0B60(user, b); }
	/* 0x0B68 */ virtual void Set_0x0B68(ObjectFieldType, xstd::vector<ItemDropMultiStruct*>*) {}
	/*        */ inline void Set(ObjectFieldType fieldType, xstd::vector<ItemDropMultiStruct*> &items) { Set_0x0B68(fieldType, &items); }
	/* 0x0B70 */ virtual void Set_0x0B70(ObjectFieldType, xstd::vector<GoodStruct*>*) {}
	/*        */ inline void Set(ObjectFieldType fieldType, xstd::vector<GoodStruct*> &items) { Set_0x0B70(fieldType, &items); }
	/* 0x0B78 */ virtual void Set_0x0B78(ObjectFieldType, xstd::vector<ItemDropStruct*>*) {}
	/*        */ inline void Set(ObjectFieldType fieldType, xstd::vector<ItemDropStruct*> &items) { Set_0x0B78(fieldType, &items); }
	/* 0x0B80 */ virtual void Set_0x0B80(ObjectFieldType, xstd::vector<double>*) {}
	/*        */ inline void Set(ObjectFieldType fieldType, xstd::vector<double> &values) { Set_0x0B80(fieldType, &values); }
	/* 0x0B88 */ virtual void Set_0x0B88(ObjectFieldType, xstd::vector<int>*) {}
	/*        */ inline void Set(ObjectFieldType fieldType, xstd::vector<int> &values) { Set_0x0B88(fieldType, &values); }
	/* 0x0B90 */ virtual void Set_0x0B90(ObjectFieldType, double) {}
	/*        */ inline void Set(ObjectFieldType fieldType, double value) { Set_0x0B90(fieldType, value); }
	/* 0x0B98 */ virtual void Set_0x0B98(ObjectFieldType, INT64) {}
	/*        */ inline void Set(ObjectFieldType fieldType, INT64 value) { Set_0x0B98(fieldType, value); }
	/* 0x0BA0 */ virtual void Set_0x0BA0(CObjectDB*, ObjectFieldType, wchar_t*) {}
	/*        */ inline void Set(CObjectDB &objectDb, ObjectFieldType fieldType, wchar_t *value) { Set_0x0BA0(&objectDb, fieldType, value); }
	/* 0x0BA8 */ virtual void vfn0x0BA8() {}
	/* 0x0BB0 */ virtual User* GetMaster_0x0BB0() { return 0; }
	/*        */ inline User* GetMaster() { return GetMaster_0x0BB0(); }
	/* 0x0BB8 */ virtual void SetMaster_0x0BB8(User*) {}
	/*        */ inline void SetMaster(User *user) { SetMaster_0x0BB8(user); }
	/* 0x0BC0 */ virtual User* GetSummoner_0x0BC0() { return 0; }
	/*        */ inline User* GetSummoner() { return GetSummoner_0x0BC0(); }
	/* 0x0BC8 */ virtual void vfn0x0BC8() {}
	/* 0x0BD0 */ virtual void OnNpcSocketClose_0x0BD0() {}
	/*        */ inline void OnNpcSocketClose() { OnNpcSocketClose_0x0BD0(); }
	/* 0x0BD8 */ virtual void vfn0x0BD8() {}

	static void Init();

	void Set_0x0B68_Wrapper(ObjectFieldType objectFieldType, xstd::vector<ItemDropMultiStruct*> *itemDrop);
	void Set_0x0B78_Wrapper(ObjectFieldType objectFieldType, xstd::vector<ItemDropStruct*> *itemDrop);
};

} // namespace l2server

