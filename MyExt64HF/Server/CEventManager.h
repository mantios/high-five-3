
#pragma once

#include <Common/Enum.h>

namespace l2server {

class CEvent;

class CEventManager {
public:
	static void Init();

	CEvent* GetEvent(GAME_EVENT::EVENT_TYPE eventType);
};

} // namespace l2server

