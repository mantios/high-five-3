
#include <windows.h>
#include <fstream>
#include <sstream>
#include <Cached/Cached.h>
#include <NPCd/NPCd.h>
#include <Server/Server.h>
#include <Common/Config.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/xstd.h>

// Add missing export
#pragma comment(lib, "IPHLPAPI.lib")
#pragma comment(linker, "/export:GetAdaptersInfo")

__declspec(dllexport) BOOL APIENTRY DllMain(HMODULE hDllModule, DWORD reason, LPVOID pReserved)
{
	(void) pReserved; // unused

	// Run initialization only on DLL_PROCESS_ATTACH (skip on DLL_PROCESS_DETACH)
	if (reason != DLL_PROCESS_ATTACH) {
		return TRUE;
	}

	// Disable further DllMain calls with DLL_THREAD_ATTACH and DLL_THREAD_DETACH
	DisableThreadLibraryCalls(hDllModule);

	// Open current process for read/write access
    server = OpenProcess(
        PROCESS_ALL_ACCESS | PROCESS_VM_READ | PROCESS_VM_WRITE,
        FALSE, GetCurrentProcessId());

	// Read signature from binary file we're attaching to
	unsigned char id[16];
	ReadMemoryBYTES(0x401000, id, 16);

	if (!memcmp(id, "\x48\x8D\x05\x11\x52\x72\x00\x48\x89\x01\xE9\x61\x72\x6B\x00\xCC", 16)) {
		// l2server
		CLog::instance = reinterpret_cast<CLog*>(0xDF218A0);
		CLog::logfn = 0x733904;
		CLog::stackfn = 0x6704E0;
		Guard::off1 = 0x7D58;
		Guard::off2 = 0x4BBF7C0;
		Guard::off3 = 0x4BE2A40;
		Assemble = reinterpret_cast<Assemble_t>(0x704E4C);
		Disassemble = reinterpret_cast<Disassemble_t>(0x705278);
		RandomDouble = reinterpret_cast<double(*)()>(0xA22A88);
		Malloc = reinterpret_cast<void*(*)(const size_t)>(0xABD840);
		Free = reinterpret_cast<void(*)(void*)>(0xABD910);
		Getc = reinterpret_cast<int(*)(FILE*)>(0xABB490);
		Ungetc = reinterpret_cast<int(*)(int, FILE*)>(0xABB3F0);
		Feof = reinterpret_cast<int(*)(FILE*)>(0xAC1F60);
		xstd::impl::Setup(0xAB8EA0, 0x74F0F4, 0xABD910, 0x74E3BC);
	    l2server::Server::Init();
	} else if (!memcmp(id, "\x48\x8D\x05\x91\x19\x21\x00\x48\x89\x01\xE9\x41\xBB\x1C\x00\xCC", 16)) {
		// cached
		CLog::instance = reinterpret_cast<CLog*>(0x2D83070);
		CLog::logfn = 0x48A5A0;
		CLog::stackfn = 0x46C8C4;
		Guard::off1 = 0x18;
		Guard::off2 = 0x2D31DD0;
		Guard::off3 = 0x2962B10;
		Assemble = reinterpret_cast<Assemble_t>(0x481B50);
		Disassemble = reinterpret_cast<Disassemble_t>(0x481F4C);
		RandomDouble = 0; // cached does not play dice
		Malloc = reinterpret_cast<void*(*)(const size_t)>(0x5CF760);
		Free = reinterpret_cast<void(*)(void*)>(0x5CF720);
		Getc = reinterpret_cast<int(*)(FILE*)>(0x5D2980);
		Ungetc = reinterpret_cast<int(*)(int, FILE*)>(0x5D28E0);
		Feof = reinterpret_cast<int(*)(FILE*)>(0x5D3440);
		xstd::impl::Setup(0x5CDAE0, 0x491500, 0x5CF720, 0x4909D4);
		cached::Cached::Init();
	} else if (!memcmp(id, "\x48\x8D\x05\xC1\x45\x19\x00\x48\x89\x01\xE9\xF1\xEF\x14\x00\xCC", 16)) {
		// l2npc
		CLog::instance = reinterpret_cast<CLog*>(0x26C3ED0);
		CLog::logfn = 0x4538F0;
		CLog::stackfn = 0x424BFC;
		Guard::off1 = 0x30F98;
		Guard::off2 = 0x2606660;
		Guard::off3 = 0x22279A0;
		Assemble = reinterpret_cast<Assemble_t>(0x44D018);
		Disassemble = reinterpret_cast<Disassemble_t>(0x44C510);
		RandomDouble = reinterpret_cast<double(*)()>(0x539BCC);
		Malloc = reinterpret_cast<void*(*)(const size_t)>(0x554240);
		Free = reinterpret_cast<void(*)(void*)>(0x5524F0);
		Getc = reinterpret_cast<int(*)(FILE*)>(0x551CE0);
		Ungetc = reinterpret_cast<int(*)(int, FILE*)>(0x551C40);
		Feof = reinterpret_cast<int(*)(FILE*)>(0x554B90);
		xstd::impl::Setup(0x5E3430, 0x4846D8, 0x5524F0, 0x4835DC);
		npc::NPCd::Init();
	} else if (!memcmp(id, "\x48\x8D\x05\x01\x33\x43\x00\x48\x89\x01\xE9\x11\xE1\x3C\x00\xCC", 16)) {
		// loader
		CLog::instance = reinterpret_cast<CLog*>(0xCA93960);
		CLog::logfn = 0x5DA8DC;
		Malloc = reinterpret_cast<void*(*)(const size_t)>(0x7D4B00);
		Free = reinterpret_cast<void(*)(void*)>(0x7D4BD0);
		WriteInstructionCall(0x78D23D, FnPtr(LoadBinaryAbsolute));
		WriteInstructionCall(0x7C6634, FnPtr(LoadBinaryAbsolute));
		WriteInstructionCall(0x7C8F02, FnPtr(LoadBinaryAbsolute));
		WriteInstructionCall(0x7C9849, FnPtr(LoadBinaryAbsolute));
		WriteMemoryBYTES(0x5D83AB, "\x49\xC7\xC0", 3);
		const wchar_t *windowTitle = L"Loader - RELEASE - patched by MyExt64HF";
		WriteMemoryDWORD(0x5D83AE, reinterpret_cast<UINT32>(windowTitle));
	} else {
		// unknown signature, refuse to run
		wchar_t buffer[1024];
		wsprintf(buffer, L"Unknown ID %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
			id[0], id[1], id[2], id[3], id[4], id[5], id[6], id[7], id[8], id[9], id[10], id[11], id[12], id[13], id[14], id[15]);
		MessageBox(0, buffer, L"Error", 0);
		return FALSE;
	}

	return TRUE;
}
