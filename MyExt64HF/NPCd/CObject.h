
#pragma once

#include <NPCd/CIOObject.h>
#include <Windows.h>

namespace npc {

class CObject : public CIOObject {
public:
	/* 0x0060 */ virtual bool Delete() { }

	/* 0x0018 */ int objectId;
	/* 0x001C */ int objectType;
	/* 0x0020 */
};

} // namespace npc

