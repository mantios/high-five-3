
#pragma once

#include <NPCd/CCreature.h>
#include <Common/CriticalSection.h>
#include <map>

class CSharedCreatureData;

namespace npc {

class CNPCEvent;

class CNPC : public CCreature {
public:
	class Ext {
	public:
		Ext();
		~Ext();

		std::map<int, std::map<int, int> > localMap;
		CriticalSection localMapCS;
	};

	static void Init();

	static CNPC* Constructor(CNPC *self);
	static CNPC* Destructor(CNPC *self, bool isMemoryFreeUsed);

	void InstantZone_Finish(int i);

	/* 0x00B8 */ virtual int HandleEvent_0x00B8(CNPCEvent*) { return 0; }
	/*        */ inline int HandleEvent(CNPCEvent *event) { return HandleEvent_0x00B8(event); }

	/* 0x0028 */ unsigned char padding0x0000[0x05C8-0x0028];
	/* 0x05C8 */ CSharedCreatureData *sm;
	/* 0x05D0 */ unsigned char padding0x05D0[0x1A58-0x05D0];
	/* 0x1A58 */ Ext ext;
};

} // namespace npc

