
#include <NPCd/Functions/GlobalObject_BroadcastAnnounce.h>
#include <NPCd/NPCServer.h>
#include <Common/CLog.h>

namespace npc {

GlobalObject_BroadcastAnnounce::GlobalObject_BroadcastAnnounce() :
	NPCFunction(L"BroadcastAnnounce", &BroadcastAnnounce)
{
}

void* GlobalObject_BroadcastAnnounce::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*)>(functionPtr.functionPtr)(
		caller, params[0]);
}

void GlobalObject_BroadcastAnnounce::SetTypes()
{
	SetReturnType(Type::TYPE_VOID);
	AddParameter(Type::TYPE_STRING);
}

int GlobalObject_BroadcastAnnounce::BroadcastAnnounce(void*, const wchar_t *text)
{
	NPCServer::Instance()->Send("chS", 0x3A, NpcExtPacket::BROADCAST_ANNOUNCE, text);
	return 0;
}

} // namespace npc

