
#include <NPCd/Functions/CNPC_IsPremium.h>
#include <Common/CSharedCreatureData.h>
#include <Common/CLog.h>

namespace npc {

CNPC_IsPremium::CNPC_IsPremium() :
	NPCFunction(L"IsPremium", &IsPremium)
{
}

void* CNPC_IsPremium::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*)>(functionPtr.functionPtr)(
		caller, params[0]);
}

void CNPC_IsPremium::SetTypes()
{
	SetReturnType(Type::TYPE_INT);
	AddParameter(Type::TYPE_CREATURE);
}

int CNPC_IsPremium::IsPremium(CNPC *npc, CSharedCreatureData *talker)
{
	return talker->isPremiumUser;
}

} // namespace npc

