
#pragma once

#include <NPCd/NPCFunction.h>
#include <NPCd/CNPC.h>
#include <Common/CSharedCreatureData.h>

namespace npc {

class CNPC_RideWyvern2 : public NPCFunction {
public:
	CNPC_RideWyvern2();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int RideWyvern2(CNPC *npc, CSharedCreatureData *talker, int classId, int duration);
};

} // namespace npc

