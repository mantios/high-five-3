
#pragma once

#include <NPCd/NPCFunction.h>
#include <Common/CSharedCreatureData.h>

namespace npc {

class GlobalObject_BroadcastAnnounce : public NPCFunction {
public:
	GlobalObject_BroadcastAnnounce();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int BroadcastAnnounce(void*, const wchar_t *text);
};

} // namespace npc

