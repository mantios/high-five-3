
#include <NPCd/Functions/CNPC_SetPremium.h>
#include <NPCd/NPCServer.h>
#include <Common/CSharedCreatureData.h>
#include <Common/CLog.h>

namespace npc {

CNPC_SetPremium::CNPC_SetPremium() :
	NPCFunction(L"SetPremium", &SetPremium)
{
}

void* CNPC_SetPremium::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*, void*)>(functionPtr.functionPtr)(
		caller, params[0], params[1]);
}

void CNPC_SetPremium::SetTypes()
{
	SetReturnType(Type::TYPE_VOID);
	AddParameter(Type::TYPE_CREATURE);
	AddParameter(Type::TYPE_INT);
}

int CNPC_SetPremium::SetPremium(CNPC *npc, CSharedCreatureData *talker, int seconds)
{
	NPCServer::Instance()->Send("chdd", 0x3A, NpcExtPacket::SET_PREMIUM, talker->index, seconds);
	return 0;
}

} // namespace npc

