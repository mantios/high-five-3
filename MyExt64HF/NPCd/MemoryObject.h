
#pragma once

namespace npc {

class MemoryObject {
public:
	/* 0x0000 */ virtual void Destruct(bool freeMemory) { }
	/* 0x0008 */ virtual MemoryObject* IncRef(const char *file, const int line, const int type) { return 0; }
	/* 0x0010 */ virtual void DecRef(const char *file, const int line, const int type, const bool unknown = false) { }
	/* 0x0018 */ virtual void vfn0x0018() { }

	/* 0x0008 */ unsigned char padding0x0008[0x0018 - 0x0008];
	/* 0x0018 */
};

} // namespace npc

