
#include <NPCd/NPCServer.h>
#include <NPCd/CServerSocket.h>
#include <stdarg.h>

namespace npc {

NPCServer* NPCServer::Instance()
{
	return reinterpret_cast<NPCServer*>(0xC0E2F0);
}

void NPCServer::Send(const char *format, ...)
{
	va_list va;
	va_start(va, format);
	socket->SendV(format, va);
	va_end(va);
}

} // namespace npc

