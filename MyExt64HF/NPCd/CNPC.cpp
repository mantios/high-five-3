
#include <NPCd/CNPC.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/CSharedCreatureData.h>

namespace npc {

void CNPC::Init()
{
	WriteMemoryDWORD(0x401136 + 8, sizeof(CNPC));
	WriteMemoryDWORD(0x407471 + 1, sizeof(CNPC));
	WriteMemoryDWORD(0x4074BC + 1, sizeof(CNPC));
	WriteMemoryDWORD(0x50A52F + 1, sizeof(CNPC));
	WriteMemoryDWORD(0x5231E5 + 1, sizeof(CNPC));
	WriteInstructionCall(0x40748B, reinterpret_cast<UINT32>(Constructor));
	WriteInstructionCall(0x4074D6, reinterpret_cast<UINT32>(Constructor));
	WriteInstructionCall(0x50A548, reinterpret_cast<UINT32>(Constructor));
	WriteInstructionCall(0x5231FF, reinterpret_cast<UINT32>(Constructor));
	WriteInstructionCall(0x4B211C, reinterpret_cast<UINT32>(Destructor));

	WriteAddress(0x4C3244 + 3, FnPtr(&CNPC::InstantZone_Finish));
}

CNPC* CNPC::Constructor(CNPC *self)
{
	CNPC *ret = reinterpret_cast<CNPC*(*)(CNPC*)>(0x4B1788)(self);
	new (&ret->ext) Ext();
	return ret;
}

CNPC::Ext::Ext()
{
}

CNPC* CNPC::Destructor(CNPC *self, bool isMemoryFreeUsed)
{
	self->ext.~Ext();
	return reinterpret_cast<CNPC*(*)(CNPC*, bool)>(0x4A3128)(self, isMemoryFreeUsed);
}

CNPC::Ext::~Ext()
{
}

void CNPC::InstantZone_Finish(int i)
{
	CLog::Add(CLog::Blue, L"NPC [%s] called InstantZone_Finish(%d)", sd->name, i);
	return reinterpret_cast<void(*)(CNPC*, int)>(0x474A44)(this, i);
}

static void check()
{
	static_assert(offsetof(CNPC, ext) == 0x1A58);
}

} // namespace npc

