
#include <NPCd/NPCd.h>
#include <NPCd/CNPC.h>
#include <NPCd/NPCFunction.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/Config.h>

namespace npc {

void NPCd::Init()
{
	// Disable sending mail to NCsoft
	DisableSendMail();

	// Hook start
	HookStart();

	// Init CNPC extensions
	CNPC::Init();

	// Init new AI functions
	NPCFunction::Init();

	// UTF-8 support
	InitUtf8Support();

	// Disable setting thread core affinity
	DontSetThreadCoreAffinity();
}

void NPCd::DisableSendMail()
{
	NOPMemory(0x423755, 5);
	NOPMemory(0x424BB6, 5);
}

void NPCd::HookStart()
{
	WriteInstructionCall(0x44FB8B, reinterpret_cast<UINT32>(StartHook));
	//NOPMemory(0x44FBDD, 6); // disable CheckMenuItem
	//NOPMemory(0x44FBFC, 6); // disable CheckMenuItem
	WriteInstructionCall(0x44FB0E, reinterpret_cast<UINT32>(CreateWindowEx), 0x44FB14); // hook window creation
}

HWND NPCd::CreateWindowEx(DWORD dwExStyle, LPCWSTR lpClassName, LPCWSTR lpWindowName, DWORD dwStyle, int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam)
{
	// Change title
	std::wstring name(lpWindowName);
	name += L" - patched by MyExt64HF";

	// Fix window out of screen
	X = 100;
	Y = 100;

	// Call original CreateWindowEx
	return ::CreateWindowEx(dwExStyle, lpClassName, name.c_str(), dwStyle, X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
}

void NPCd::StartHook(void *logger, int level, const char *fmt)
{
	// Call original logging
	reinterpret_cast<void(*)(void*, int, const char*)>(0x454468)(logger, level, fmt);

	// Add our log message
	CLog::Add(CLog::Blue, L"Patched by MyExt64HF");	

	// Move old dumps to backup directory
	ShellExecute(0, L"open", L"cmd.exe", L"/C mkdir bak", 0, SW_HIDE);
	ShellExecute(0, L"open", L"cmd.exe", L"/C move LinError.txt.*.bak bak\\", 0, SW_HIDE);
	ShellExecute(0, L"open", L"cmd.exe", L"/C move minidump_*.dmp bak\\", 0, SW_HIDE);
}

void NPCd::DontSetThreadCoreAffinity()
{
	NOPMemory(0x44E957, 6); // let OS decide which thread will execute on which core
}

namespace {

template<UINT32 consumeBomAddress>
int GetCharForLexer(void **stream)
{
	static bool utf8 = false;
	void *obj = stream[14];
	if (!obj) return -1;
	int *consumeBom = reinterpret_cast<int*>(consumeBomAddress);
	bool characterAlreadyConsumed = false;
	unsigned char c1 = 0;
	if (*consumeBom) {
		*consumeBom = 0;
		c1 = reinterpret_cast<wchar_t(*)(void*)>(0x53D320)(obj);
		{
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) return -1;
		}
		if (c1 == 0xFF) {
			if (reinterpret_cast<wchar_t(*)(void*)>(0x53D320)(obj) != 0xFE) {
				CLog::Add(CLog::Blue, L"wrong utf-16le bom");
				return -1;
			}
			utf8 = false;
		} else if (c1 == 0xEF) {
			if (reinterpret_cast<wchar_t(*)(void*)>(0x53D320)(obj) != 0xBB) {
				CLog::Add(CLog::Blue, L"wrong utf-8 bom (1)");
				return -1;
			}
			{
				bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
				if (*fail) return -1;
			}
			if (reinterpret_cast<wchar_t(*)(void*)>(0x53D320)(obj) != 0xBF) {
				CLog::Add(CLog::Blue, L"wrong utf-8 bom (2)");
				return -1;
			}
			{
				bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
				if (*fail) return -1;
			}
			utf8 = true;
		} else {
			utf8 = true;
			characterAlreadyConsumed = true;
		}
	}
	if (!utf8) {
		unsigned char c1 = reinterpret_cast<wchar_t(*)(void*)>(0x53D320)(obj);
		{
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) return -1;
		}
		unsigned char c2 = reinterpret_cast<wchar_t(*)(void*)>(0x53D320)(obj);
		{
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) return -1;
		}
		return c1 | (c2 << 8);
	} else {
		unsigned char c;
		if (characterAlreadyConsumed) {
			c = c1;
		} else {
			c = reinterpret_cast<wchar_t(*)(void*)>(0x53D320)(obj);
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) {
				return -1;
			}
		}
		if (c < 0x80) return c;
		if (c < 0xC0) {
			CLog::Add(CLog::Red, L"Can't decode UTF-8 (invalid character)");
			return -1;
		}
		size_t chars(0);
		wchar_t wc(0);
		if (c >= 0xFC) {
			wc = c & 0x01;
			chars = 5;
		} else if (c >= 0xF8) {
			wc = c & 0x03;
			chars = 4;
		} else if (c >= 0xF0) {
			wc = c & 0x07;
			chars = 3;
		} else if (c >= 0xE0) {
			wc = c & 0x0F;
			chars = 2;
		} else {
			wc = c & 0x1F;
			chars = 1;
		}
		for (; chars; --chars) {
			wc <<= 6;
			c = reinterpret_cast<wchar_t(*)(void*)>(0x53D320)(obj);
			wc |= c & 0x3f;
			bool *fail = reinterpret_cast<bool*>(&reinterpret_cast<char*>(obj)[reinterpret_cast<UINT32**>(obj)[0][1] + 0x10]);
			if (*fail) {
				CLog::Add(CLog::Red, L"Can't decode UTF-8 (end of input)");
				return -1;
			}
		}
		return wc;
	}
}

} // namespace

void NPCd::InitUtf8Support()
{
	WriteInstructionCall(0x53ACC0, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x53ADA9, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x539F11, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x53FD39, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x4FA4EA, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x4FA6BC, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x4FA975, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x53A826, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x53A840, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x53A86C, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x53A90C, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x53A948, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x4F8DCE, FnPtr(ReadNextWchar));
	WriteInstructionCall(0x4F8E13, FnPtr(ReadNextWchar));
	WriteMemoryQWORD(0x62E228, FnPtr(GetCharForLexer<0x69B49C>));
	WriteMemoryQWORD(0x62EC48, FnPtr(GetCharForLexer<0x69B4F8>));
	WriteMemoryQWORD(0x637B98, FnPtr(GetCharForLexer<0x69B600>));
}

} // namespace npc

