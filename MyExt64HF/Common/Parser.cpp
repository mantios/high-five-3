
#include <Common/Parser.h>
#include <Common/codecvt_ucs2.h>
#include <Common/codecvt_utf8.h>
#include <Common/CLog.h>
#include <fstream>
#include <locale>

SimpleSkipperParser::SimpleSkipperParser() : SimpleSkipperParser::base_type(start)
{
	start %= wide::char_(L" \t\r\n");
}

SimpleParser::SimpleParser() : SimpleParser::base_type(start)
{
}

bool SimpleParser::Parse(const std::wstring &data, const std::wstring &error, const bool fatal)
{
	SimpleParserIterator posIterator(data.begin(), data.end());
	SimpleParserIterator posEnd;
	Skipper skipper;
	try {
		qi::phrase_parse(posIterator, posEnd, *this, skipper);
		return true;
	} catch (const qi::expectation_failure<Iterator> &e) {
		const boost::spirit::classic::file_position_base<std::wstring> &pos(e.first.get_position());
		if (fatal) {
			wchar_t buffer[1024];
			_snwprintf(buffer, 1024, L"%s at line %d, column %d", error.c_str(), pos.line, pos.column);
			MessageBoxW(0, buffer, L"Fatal error", MB_ICONSTOP);
			exit(1);
		} else {
			CLog::Add(CLog::Red, L"%s at line %d, column %d", error.c_str(), pos.line, pos.column);
			return false;
		}
	}
}

SkipperParser::SkipperParser() : SkipperParser::base_type(start)
{
	start %= whitespace | lineComment | blockComment;
	whitespace %= wide::char_(L" \t\r\n");
	lineComment %= wide::string(L"//") > *(wide::char_ - qi::eol) > qi::eol;
	blockComment %= wide::string(L"/*") > *(wide::char_ - wide::string(L"*/")) > wide::string(L"*/");
}

Parser::Parser() : Parser::base_type(start)
{
	bom %= qi::no_skip[wide::char_(0xFEFF)];
	identifier %= qi::as_wstring[wide::alpha > *(wide::alnum | wide::char_(L"_"))];
	integer %= boost::spirit::long_;
	decimal %= boost::spirit::double_;
	lbracket %= wide::char_(L'{');
	rbracket %= wide::char_(L'}');
	semicolon %= wide::char_(L';');
	assign %= wide::char_(L'=');
	quote %= qi::char_('"');
	quotedstring %= qi::omit[quote] > qi::lexeme[
		qi::raw[*((wide::char_ - wide::char_(L'\\') - wide::char_(L'"')) | (wide::char_(L'\\') > wide::char_))]
	] > qi::omit[quote];
	bracketstring %= qi::omit[wide::char_(L'[')] > qi::lexeme[
		qi::raw[*((wide::char_ - wide::char_(L'\\') - wide::char_(L']')) | (wide::char_(L'\\') > wide::char_))]
	] > qi::omit[wide::char_(L']')];

}

bool Parser::Parse(const std::wstring &filename, const bool errorIfCantOpen)
{
	std::wifstream input(filename.c_str(), std::ios::binary);
	if (!input) {
		if (errorIfCantOpen) {
			CLog::Add(CLog::Red, L"Can't open file %s", filename.c_str());
		}
		return !errorIfCantOpen;
	}
	CLog::Add(CLog::Blue, L"Read %s", filename.c_str());
	bool utf8 = true;
	wchar_t bom[2];
	bom[0] = bom[1] = 0;
	input.read(bom, 2);
	if (bom[0] == 0xFF && bom[1] == 0xFE) utf8 = false;
	input.seekg(0, std::ios::beg);
	if (utf8) {
		input.imbue(std::locale(input.getloc(), new codecvt_utf8()));
	} else {
		input.imbue(std::locale(input.getloc(), new codecvt_ucs2()));
	}
	input.unsetf(std::ios::skipws);

	ParserStreamIterator iterator(input);
	ParserMultiIterator multiIterator = boost::spirit::make_default_multi_pass(iterator);
	ParserMultiIterator multiEnd;

	ParserIterator posIterator(multiIterator, multiEnd);
	ParserIterator posEnd;

	Skipper skipper;

	try {
		qi::phrase_parse(posIterator, posEnd, *this, skipper);
		return true;
	} catch (const qi::expectation_failure<Iterator> &e) {
		const boost::spirit::classic::file_position_base<std::wstring> &pos(e.first.get_position());
		CLog::Add(CLog::Red, L"Error parsing file %s at line %d column %d", filename.c_str(), pos.line, pos.column);
		return false;
	}
}

