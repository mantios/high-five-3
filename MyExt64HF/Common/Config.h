
#pragma once

#include <string>
#include <set>
#include <vector>
#include <windows.h>
#include <Common/CriticalSection.h>

class Config {
public:
	// Returns config instance, if it doesn't exist yet, it creates it
	static Config* Instance();

	// Reloads data from configuration file
	void Reload();

	// Basic server settings
	struct Server {
		void Load(Config *config);

		// Server name
		std::wstring name;

		// Server debug mode
		bool debug;

		// Server deadlock timeout in seconds
		time_t deadlockTimeout;

		// Server shutdown duration in seconds
		time_t shutdownDuration;

		// Whether use one script directory for everything
		bool oneScriptDirectory;

		// Whether automatically remove GM from GM list on logout
		bool autoRemoveFromGMList;

		// Plugin DLL to load
		std::wstring plugin;

		// Whether allow skills on airships
		bool allowSkillsOnAirship;

		// Whether to keep songs/dances on /mount
		bool mountKeepSongsDances;

		// Whether to keep songs/dances on /dismount
		bool dismountKeepSongsDances;
	} *server;

	// Voice command settings
	struct VoiceCommands {
		void Load(Config *config);

		// Enable voice commands?
		bool enabled;

		// Enable expon/expoff commands?
		bool expOnOff;

		// Enable petexpon/petexpoff commands?
		bool petExpOnOff;

		// Enabled offline command?
		bool offline;

		// Enable time command?
		bool time;
	} *voiceCommands;

	// Various configurable fixes
	struct Fixes {
		void Load(Config *config);

		// When enabled, command channel members are friends, not neutral/enemies
		bool commandChannelFriendly;

		// Whether disallow sending /trade on Olympiad
		bool disallowTradeInOlympiad;

		// Whether keep songs and dances over relog
		bool relogKeepSongsDances;

		// Whether to force Olympiad to run monthly even when time zone changes (It's still not recommended to change time zones or use DST!)
		bool forceMonthlyOlympiad;

		// Detect multisell IDs from HTMLs sent to user and allow use only those that were seen
		bool multisellFiltering; // TODO: check if needed
	} *fixes;

	// Rate changes
	struct Rate {
		void Load(Config *config);

		// Adena drop rate multiplier
		double adenaRate;

		// Drop rate multiplier
		double dropRate;

		// Spoil rate multiplier
		double spoilRate;

		// Boss drop rate multiplier
		double bossDropRate;

		// Herb drop rate multiplier
		double herbRate;

		// Fixup for low levels - increase drop and spoil rate progresivelly between levels 1 and 20
		bool fixupLowLevel;

		// Items that aren't affected by rate changes
		std::set<INT64> ignoredItems;

		// Whether dump drop/spoil info to dump/dropspoil.txt file
		bool dump;

		// Seal stone amount rate multiplier
		double sealStoneAmountRate;
	} *rate;

	// Alternative clan restrictions
	struct ClanRestrictions {
		void Load(Config *config);

		// All in seconds
		UINT32 pledgeWarTimeout;
		UINT32 pledgeOustPenaltyTimeout;
		UINT32 pledgeWithdrawPenaltyTimeout;
		UINT32 pledgeOustedPenaltyTimeout;
		UINT32 pledgeRechallengePenaltyTimeout;
		UINT32 pledgeRechallengeDespiteRejectionPenaltyTimeout;
		UINT32 pledgeDismissTimeout;
		UINT32 pledgeDismissPenaltyTimeout;
		UINT32 pledgeDismissByNPC;
		UINT32 allianceCanAcceptNewMemberPledge;
		UINT32 allianceOustPenaltyTimeout;
		UINT32 allianceWithdrawPenaltyTimeout;
		UINT32 allianceOustedPenaltyTimeout;
		UINT32 allianceDismissPenaltyTimeout;
		UINT32 allianceRechallengePenaltyTimeout;
		UINT32 allianceRechallengeDespiteRejectionPenaltyTimeout;
		UINT32 castleAnnounceTime;
		UINT32 castleStandbyTime;
	} *clanRestrictions;

	// Changes in buff system
	struct BuffSystem {
		void Load(Config *config);

		// Maximum buff slot count
		int maxSlots;

		// Maximum ddivine inspiration bonus slots
		int maxDivineInspirationBonusSlots;
	} *buffSystem;

	// Changes in olympiad system
	struct Olympiad {
		void Load(Config *config);

		// Minimum team count to start team olympiad
		int entryCountTeam;

		// Minimum character count to start non-class olympiad
		int entryCountNonclass;

		// Minimum character count to start class olympiad
		int entryCountClass;
	} *olympiad;

	// Autoloot
	struct AutoLoot {
		void Load(Config *config);

		// Whether autoloot from mobs is enabled
		bool autoLootMobDrop;

		// Whether autoloot from bosses is enabled
		bool autoLootBossDrop;

		// Maximum distance to loot monster
		int maximumAutoLootDistance;

		// IDs of items excluded from autoloot
		std::set<INT64> excludedItems;
	} *autoLoot;

	// Beta features
	struct Beta {
		void Load(Config *config);

		// Beta enabled?
		bool enabled;

		// Level change enabled?
		bool level;

		// Class change enabled?
		bool class_;

		// Adena giving enabled?
		bool adena;

		// Free noblesse enabled?
		bool noblesse;

		// Free SP enabled?
		bool sp;

		// Free fame enabled?
		bool fame;
	} *beta;

	// Custom features
	struct Custom {
		void Load(Config *config);

		// Minimum distance between private stores
		int minShopDistance;

		// When set to true, pets never drop items (they go to owner inventory instead)
		bool dontDropPetItems;

		// Whether keep pet food in pet inventory (e.g. not return it to owner)
		bool keepFoodInPetInventory;

		// Whether allow multiple attributes on an item
		bool allowMultipleAttributesOnItem;

		// Whether calculate attribute bonuses like Epilogue (true) or H5 (false)
		bool epilogueAttributeBonuses;

		// Whether disable navit system
		bool disableNavitSystem;

		// Wheter keep reuse bug working
		bool dontFixReuseBug;

		// Skill land probability min/max, 10.0/90.0 for H5, 0.0/95.0 for Gracia
		double skillLandProbMin;
		double skillLandProbMax;

		// Maximum normal buff slots (not counting Divine Inspiration)
		int maxNormalBuffSlots;

		// Maximum song/dance slots
		int maxSongDanceSlots;
	} *custom;

	// NPCd features
	struct Npcd {
		void Load(Config *config);

		// Whether use Split AI instead of ai.obj
		bool useSplitAI;

		// Size of AI buffer for Split AI
		size_t aiBufferSizeMB;

		// Whether dump Split AI to dump/ai.obj file
		bool dumpSplitAI;
	} *npcd;

	// Miscellaneous settings
	struct Misc {
		void Load(Config *config);

		// Whether change name color to grey when entering offline trade
		bool offlineTradeChangeNameColor;
	} *misc;

	struct Charsets {
		void Load(Config *config);

		std::set<wchar_t> allowedCharacterNameLetters;
	} *charsets;

protected:
	Config(const wchar_t *filename);

	// Get string value from config
	std::wstring GetString(const wchar_t *section, const wchar_t *name, const wchar_t *defaultValue);

	// Get integer value from config
	INT64 GetInt(const wchar_t *section, const wchar_t *name, const INT64 defaultValue);

	// Get boolean value from config (accepts 0/false/off/no and 1/true/on/yes)
	bool GetBool(const wchar_t *section, const wchar_t *name, const bool defaultValue);

	// Get double value from config
	double GetDouble(const wchar_t *section, const wchar_t *name, const double defaultValue);

	// Get integer list from config
	std::vector<INT64> GetIntList(const wchar_t *section, const wchar_t *name, const std::vector<INT64> &defaultValue);

	// Get integer set from config
	std::set<INT64> GetIntSet(const wchar_t *section, const wchar_t *name, const std::set<INT64> &defaultValue);

	// Configuration file path
	std::wstring filename;

	// Config instance
	static Config *instance;

	// Critical section
	static CriticalSection instanceCS;
};

