
#pragma once

#include <windows.h>
#include <stdarg.h>

class CLog {
public:
	// Log types
	static const int Red;
	static const int Blue;
	static const int In;

	// When ORed to type, also logs call stack
	static const int CallStack;

	// Add log message
	static void Add(const int type, const wchar_t* format, ...);

	// Add log message - va_list variant
	static void AddV(const int type, const wchar_t *format, va_list va);

	// Log just callstack
	static void LogCallStack(const int type);

	// Add debug message (only logs it to log when debug is enabled)
	static void Debug(const int type, const wchar_t* format, ...);

	// Add debug message - va_list variant (only logs it to log when debug is enabled)
	static void DebugV(const int type, const wchar_t *format, va_list va);

	// Logger instance, set in main.cpp (different for cached/l2server/l2npc)
	static CLog *instance;

	// Logging function, set in main.cpp (different for cached/l2server/l2npc)
	static UINT32 logfn;

	// Callstack logging function, set in main.cpp (different for cached/l2server/l2npc)
	static UINT32 stackfn;
};

