
#pragma once

#include <Common/CSharedData.h>
#include <Common/Enum.h>

class CSharedCreatureData : public CSharedData {
public:
	/* 0x0040 */ unsigned char padding0x0040[0x0080 - 0x0040];
	/* 0x0080 */ int gender;
	/* 0x0084 */ bool undying;
	/* 0x0085 */ bool attackable;
	/* 0x0086 */ bool hidden;
	/* 0x0087 */ unsigned char padding0x0087[0x0158 - 0x0087];
	/* 0x0158 */ wchar_t name[25];
	/* 0x018A */ unsigned char padding0x018A[0x01A0 - 0x018A];
	/* 0x01A0 */ int stopMode;
	/* 0x01A4 */ unsigned char padding0x01A4[0x01B8 - 0x01A4];
	/* 0x01B8 */ int guilty;
	/* 0x01BC */ unsigned char padding0x01BC[0x01C0 - 0x01BC];
	/* 0x01C0 */ int pkCounter;
	/* 0x01C4 */ int pkPardon;
	/* 0x01C8 */ int karma;
	/* 0x01CC */ unsigned char padding0x01CC[0x18D4 - 0x01CC];
	/* 0x18D4 */ int partyId;
	/* 0x18D8 */ unsigned char padding0x18D8[0x18F4 - 0x18D8];
	/* 0x18F4 */ PrivateStoreType privateStoreType;
	/* 0x18F8 */ unsigned char padding0x18F8[0x19EC - 0x18F8];
	/* 0x19EC */ int level;
	/* 0x19F0 */ unsigned char padding0x19F0[0x1B18 - 0x19F0];
	/* 0x1B18 */ int builder;
	/* 0x1B1C */ unsigned char padding0x1B1C[0x1B98 - 0x1B1C];
	/* 0x1B98 */ bool isPremiumUser;
	/* 0x1B99 */ unsigned char padding0x1B99[0x1BAC - 0x1B99];
	/* 0x1BAC */ bool inCombat;
	/* 0x1BAD */ unsigned char padding0x1BAD[0x1D5C - 0x1BAD];
	/* 0x1D5C */ int yongmaType;
	/* 0x1D60 */ unsigned char padding0x1D60[0x1DD7 - 0x1D60];
	/* 0x1DD7 */ bool visible;
	/* 0x1DD8 */ unsigned char padding0x1DD8[0x1DE0 - 0x1DD8];
	/* 0x1DE0 */ int airshipId;
	/* 0x1DE4 */ unsigned char padding0x1DE4[0x2170 - 0x1DE4];
	/* 0x2170 */

};

