
#include <Common/codecvt_ucs2.h>

codecvt_ucs2::result codecvt_ucs2::do_in(mbstate_t&,
	const char *from, const char *fromEnd, const char *&fromNext,
	wchar_t *to, wchar_t *toEnd, wchar_t *&toNext) const
{
	size_t maxInput = (fromEnd - from) & ~1;
	size_t maxOutput = (toEnd - to);
	size_t count = std::min(maxInput / 2, maxOutput);
	result res = ok;
	fromNext = from;
	toNext = to;
	for (; count--; fromNext += 2, ++toNext) {
		unsigned char c1 = *fromNext, c2 = *(fromNext + 1);
		*toNext = c1 | c2 << 8;
	}
	if (toNext == to && fromNext == fromEnd - 1) res = partial;
	return res;
}

codecvt_ucs2::result codecvt_ucs2::do_out(mbstate_t&,
	const wchar_t *from, const wchar_t *fromEnd, const wchar_t *&fromNext,
	char *to, char *toEnd, char *&toNext) const
{
	size_t maxInput = (fromEnd - from);
	size_t maxOutput = (toEnd - to) & ~1;
	size_t count = std::min(maxInput, maxOutput / 2);
	fromNext = from;
	toNext = to;
	for (; count--; ++fromNext, toNext += 2) {
		*(toNext + 0) = (char) (*fromNext & 0xFF);
		*(toNext + 1) = (char) (*fromNext >> 8 & 0xFF);
	}
	return ok;
}

bool codecvt_ucs2::do_always_noconv() const throw()
{
	return false;
}

int codecvt_ucs2::do_encoding() const throw()
{
	return 2;
}

