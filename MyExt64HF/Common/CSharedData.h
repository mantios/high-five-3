
#pragma once

#include <Common/Utils.h>

class CSharedData {
public:
	/* 0x0000 */ unsigned char padding0x0000[0x0008 - 0x0000];
	/* 0x0008 */ FVector location;
	/* 0x0020 */ unsigned char padding0x0020[0x0028 - 0x0020];
	/* 0x0028 */ int index;
	/* 0x002C */ int containerIndex;
	/* 0x0030 */ unsigned char padding0x0030[0x0034 - 0x0030];
	/* 0x0034 */ bool inWorld;
	/* 0x0035 */ unsigned char padding0x0035[0x0040 - 0x0035];
};

