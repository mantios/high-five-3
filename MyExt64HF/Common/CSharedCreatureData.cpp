
#include <Common/CSharedCreatureData.h>
#include <Common/Utils.h>

static void check()
{
	static_assert(offsetof(CSharedCreatureData, gender) == 0x80);
	static_assert(offsetof(CSharedCreatureData, name) == 0x158);
	static_assert(offsetof(CSharedCreatureData, padding0x1DE4) == 0x1DE4);
}

