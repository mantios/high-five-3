
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <algorithm>

HANDLE server = NULL;

void WriteMemoryBYTES(unsigned int address, void *bytes, unsigned int len)
{
	DWORD flOldProtect;
	SIZE_T uNumberOfBytesWritten;
	HANDLE handle;
	
	if ((handle = server) && len) {
		VirtualProtectEx(handle, (LPVOID)address, len, PAGE_WRITECOPY, &flOldProtect);
		WriteProcessMemory(handle, (LPVOID)address, bytes, len, &uNumberOfBytesWritten);
		FlushInstructionCache(handle, (LPVOID)address, len);
		VirtualProtectEx(handle, (LPVOID)address, len, flOldProtect, &flOldProtect);
	}
}

void MakeExecutable(unsigned int address, unsigned int len)
{
	DWORD flOldProtect;
	HANDLE handle;
	if ((handle = server) && len) {
		VirtualProtectEx(handle, (LPVOID)address, len, PAGE_EXECUTE, &flOldProtect);
	}
}

void ReadMemoryBYTES(unsigned int address, void *bytes, unsigned int len)
{
	DWORD flOldProtect;
	SIZE_T uNumberOfBytesRead;
	HANDLE handle;
	
	if ((handle = server) && len) {
		VirtualProtectEx(handle, (LPVOID)address, len, PAGE_WRITECOPY, &flOldProtect);
		ReadProcessMemory(handle, (LPVOID)address, bytes, len, &uNumberOfBytesRead);
		FlushInstructionCache(handle, (LPVOID)address, len);
		VirtualProtectEx(handle, (LPVOID)address, len, flOldProtect, &flOldProtect);
	}
}

void WriteMemoryQWORD(unsigned int address, unsigned __int64 value)
{
    WriteMemoryBYTES(address, &value, sizeof(unsigned __int64));
}

void WriteMemoryDWORD(unsigned int address, unsigned int value)
{
    WriteMemoryBYTES(address, &value, sizeof(unsigned int));
}

void WriteMemoryWORD(unsigned int address, unsigned short value)
{
    WriteMemoryBYTES(address, &value, sizeof(unsigned short));
}

void WriteMemoryBYTE(unsigned int address, unsigned char value)
{
    WriteMemoryBYTES(address, &value, sizeof(unsigned char));
}

unsigned __int64 ReadMemoryQWORD(unsigned int address)
{
    unsigned __int64 value;
    ReadMemoryBYTES(address, &value, sizeof(unsigned __int64));
    return value;
}

unsigned int ReadMemoryDWORD(unsigned int address)
{
    unsigned int value;
    ReadMemoryBYTES(address, &value, sizeof(unsigned int));
    return value;
}

unsigned short ReadMemoryWORD(unsigned int address)
{
    unsigned short value;
    ReadMemoryBYTES(address, &value, sizeof(unsigned short));
    return value;
}

unsigned char ReadMemoryBYTE(unsigned int address)
{
    unsigned char value;
    ReadMemoryBYTES(address, &value, sizeof(unsigned char));
    return value;
}

void NOPMemory(unsigned int address, unsigned int len)
{
	unsigned int dword_count = (len / 4), byte_count = (len % 4);
	unsigned char Byte = 0x90; 
	unsigned int Dword = 0x90666666;

	DWORD flOldProtect;
	SIZE_T uNumberOfBytesWritten;
	HANDLE handle;

	if ((handle = server) && len) {
		VirtualProtectEx(handle, (LPVOID)address, len, PAGE_WRITECOPY, &flOldProtect);
		while (dword_count) {
			WriteProcessMemory(handle, (LPVOID)address, &Dword, sizeof(unsigned int), &uNumberOfBytesWritten);
			address += sizeof(unsigned int);
			dword_count--;
		}
		while(byte_count) {
			WriteProcessMemory(handle, (LPVOID)address, &Byte, sizeof(unsigned char), &uNumberOfBytesWritten);
			address += sizeof(unsigned char);
			byte_count--;
		}
		FlushInstructionCache(handle, (LPVOID)address, len);
		VirtualProtectEx(handle, (LPVOID)address, len, flOldProtect, &flOldProtect);
	}
}

void NULLMemory(unsigned int address, unsigned int len)
{
	unsigned int dword_count = (len / 4), byte_count = (len % 4);
	unsigned char Byte = 0x00; 
	unsigned int Dword = 0x00000000;

	DWORD flOldProtect;
	SIZE_T uNumberOfBytesWritten;
	HANDLE handle;

	if((handle = server) && len) {
		VirtualProtectEx(handle, (LPVOID)address, len, PAGE_WRITECOPY, &flOldProtect);
		while(dword_count) {
			WriteProcessMemory(handle, (LPVOID)address, &Dword, sizeof(unsigned int), &uNumberOfBytesWritten);
			address += sizeof(unsigned int);
			dword_count--;
		}
		while(byte_count) {
			WriteProcessMemory(handle, (LPVOID)address, &Byte, sizeof(unsigned char), &uNumberOfBytesWritten);
			address += sizeof(unsigned char);
			byte_count--;
		}
		FlushInstructionCache(handle, (LPVOID)address, len);
		VirtualProtectEx(handle, (LPVOID)address, len, flOldProtect, &flOldProtect);
	}
}

void WriteInstruction(unsigned int address, unsigned int uDestination, unsigned char uFirstByte)
{
	unsigned char execLine[5];
	execLine[0] = uFirstByte;
	*((int*)(execLine + 1)) = (((int)uDestination) - (((int)address) + 5));
	WriteMemoryBYTES(address, execLine, 5);
}

void WriteInstructionCallJmpEax(unsigned int address, unsigned int uDestination, unsigned int uNopEnd)
{
	unsigned char execLine[7];
	execLine[0] = 0xE8;
	*((int*)(execLine + 1)) = (((int)uDestination) - (((int)address) + 5));
	*((unsigned short*)(execLine + 5)) = 0xE0FF;
	WriteMemoryBYTES(address, execLine, 7);
	if(uNopEnd && uNopEnd > (address + 7))
		NOPMemory((address + 7), (uNopEnd - (address + 7)));
}

void WriteInstructionCall(unsigned int address, unsigned int uDestination, unsigned int uNopEnd)
{
	unsigned char execLine[5];
	execLine[0] = 0xE8;
	*((int*)(execLine + 1)) = (((int)uDestination) - (((int)address) + 5));
	WriteMemoryBYTES(address, execLine, 5);
	if(uNopEnd && uNopEnd > (address + 5))
		NOPMemory((address + 5), (uNopEnd - (address + 5)));
}

void WriteInstructionJmp(unsigned int address, unsigned int uDestination, unsigned int uNopEnd)
{
	unsigned char execLine[5];
	execLine[0] = 0xE9;
	*((int*)(execLine + 1)) = (((int)uDestination) - (((int)address) + 5));
	WriteMemoryBYTES(address, execLine, 5);
	if(uNopEnd && uNopEnd > (address + 5))
		NOPMemory((address + 5), (uNopEnd - (address + 5)));
}

bool ReplaceString(unsigned int address, const wchar_t *from, const wchar_t *to)
{
    int fromLen = (int) wcslen(from);
    int delta = fromLen - (int) wcslen(to);
    if (delta < 0) {
        return false;
    }
    const wchar_t *pos = wcsstr((const wchar_t*) address, from);
    if (!pos) {
        return false;
    }
    for (int i = 0 ; pos[delta] ; ++pos, ++i) {
        WriteMemoryWORD((UINT32)pos, i < fromLen - delta ? to[i] : pos[delta]);
    }
    WriteMemoryWORD((UINT32)pos, 0);
    return true;
}

void WriteAddress(unsigned int uAddress, UINT32 absAddr)
{
	WriteMemoryDWORD(uAddress, absAddr - (uAddress + 4));
}

Assemble_t Assemble = 0;
Disassemble_t Disassemble = 0;
void*(*Malloc)(const size_t) = 0;
void(*Free)(void*) = 0;

UINT32 GetThreadIndex()
{
	return *reinterpret_cast<UINT32*>(*reinterpret_cast<UINT64*>(__readgsqword(0x58)) + Guard::off1);
}

int GetGlobalWorldId()
{
	return *reinterpret_cast<int*>(0x1312EDC);
}

int GetWorldId()
{
	return *reinterpret_cast<int*>(*reinterpret_cast<UINT64*>(__readgsqword(0x58)) + 0x3E94);
}

void SetWorldId(const int id)
{
	*reinterpret_cast<int*>(*reinterpret_cast<UINT64*>(__readgsqword(0x58)) + 0x3E94) = id;
}

int GetConnectionType()
{
	return *reinterpret_cast<int*>(*reinterpret_cast<UINT64*>(__readgsqword(0x58)) + 0x10);
}

WorldGuard::WorldGuard(const int userWorldId) : oldWorldId(GetWorldId())
{
	if (GetThreadIndex() >= 8 || userWorldId < 0) {
		SetWorldId(GetGlobalWorldId());
	} else {
		SetWorldId(userWorldId);
	}
}

WorldGuard::~WorldGuard()
{
	SetWorldId(oldWorldId);
}

std::basic_string<wchar_t> Widen(const std::string &s)
{
	std::wstring result;
	std::copy(s.begin(), s.end(), std::back_inserter(result));
	return result;
}

std::string Narrow(const std::basic_string<wchar_t> &s)
{
	std::string result;
	for (size_t i = 0 ; i < s.size() ; ++i) {
		result.push_back(static_cast<char>(s[i]));
	}
	return result;
}

size_t Guard::off1 = 0;
size_t Guard::off2 = 0;
size_t Guard::off3 = 0;

bool Guard::WasCalled(const wchar_t *namePtr)
{
	if (!off1 || !off2 || !off3) return false;
	UINT32 threadIndex = GetThreadIndex();
	for (size_t i = 0 ; i < reinterpret_cast<UINT32*>(off3)[threadIndex] ; ++i) {
		if (reinterpret_cast<const wchar_t**>(off2)[threadIndex * 1000 + i] == namePtr) {
			return true;
		}
	}
	return false;
}

std::pair<unsigned char*, size_t> ReadWholeFile(const wchar_t *filename)
{
	HANDLE h = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (!h) {
		return std::pair<unsigned char*, size_t>(0, 0);
	}
	DWORD size = GetFileSize(h, 0);
	DWORD res = 0;
	unsigned char *buffer = new unsigned char[size];
	ReadFile(h, buffer, size, &res, NULL);
	if (res != size) {
		CloseHandle(h);
		delete [] buffer;
		return std::pair<unsigned char*, size_t>(0, 0);
	}
	CloseHandle(h);
	return std::pair<unsigned char*, size_t>(buffer, size);
}

std::vector<std::string> Tokenize(const std::string &text)
{
	std::vector<std::string> result;
	std::string part;
	for (size_t i = 0 ; i < text.size() ; ++i) {
		bool white = (text[i] == ' ' || text[i] == '\t' || text[i] == '\r' || text[i] == '\n');
		if (!white) {
			part.push_back(text[i]);
		}
		if ((white || i == text.size() - 1) && !part.empty()) {
			result.push_back(part);
			part.clear();
		}
	}
	return result;
}

double(*RandomDouble)();

unsigned char* LoadBinaryAbsolute(const wchar_t *filename, int *length)
{
	std::string filenameString;
	for (size_t i = 0; filename[i]; ++i) filenameString.push_back(filename[i]);
	std::ifstream ifs(filenameString.c_str(), std::ios::binary);
	if (!ifs) return 0;
	unsigned char c = ifs.get();
	size_t startOffset = 0;
	if (!ifs) {
		unsigned char *buf = reinterpret_cast<unsigned char*>(Malloc(2));
		buf[0] = 0xFF;
		buf[1] = 0xFE;
		*length = 2;
		return buf;
	}
	if (c == 0xFF) {
		c = ifs.get();
		if (ifs && c == 0xFE) {
			ifs.seekg(0, std::ios::end);
			*length = ifs.tellg();
			ifs.seekg(0, std::ios::beg);
			unsigned char *buf = reinterpret_cast<unsigned char*>(Malloc(*length + 0x50));
			ifs.read(reinterpret_cast<char*>(buf), *length);
			ifs.close();
			return buf;
		}
	} else if (c == 0xEF) {
		c = ifs.get();
		if (ifs && c == 0xBB) {
			c = ifs.get();
			if (ifs && c == 0xBF) {
				startOffset = 3;
			}
		}
	}
	ifs.seekg(0, std::ios::end);
	size_t size = ifs.tellg();
	size -= startOffset;
	ifs.seekg(startOffset, std::ios::beg);
	unsigned char *buf = reinterpret_cast<unsigned char*>(Malloc(size * 2 + 0x50 + 2));
	buf[0] = 0xFF;
	buf[1] = 0xFE;
	wchar_t *ptr = reinterpret_cast<wchar_t*>(buf) + 1;
	for (size_t i = 0; i < size;) {
		unsigned char c = static_cast<unsigned char>(ifs.get());
		++i;
		if (c < 0x80) {
			*ptr++ = c;
			continue;
		}
		if (c < 0xC0) {
			CLog::Add(CLog::Red, L"Can't decode UTF-8 in file %s at position %d: invalid char %02X", filename, i + startOffset, c);
			Free(buf);
			return 0;
		}
		size_t chars(0);
		wchar_t wc(0);
		if (c >= 0xFC) {
			wc = c & 0x01;
			chars = 5;
		} else if (c >= 0xF8) {
			wc = c & 0x03;
			chars = 4;
		} else if (c >= 0xF0) {
			wc = c & 0x07;
			chars = 3;
		} else if (c >= 0xE0) {
			wc = c & 0x0F;
			chars = 2;
		} else {
			wc = c & 0x1F;
			chars = 1;
		}
		for (; chars; --chars, ++i) {
			if (i >= size) {
				CLog::Add(CLog::Red, L"Can't decode UTF-8 in file %s at position %d: end of file", filename, i + startOffset);
				Free(buf);
				return 0;
			}
			wc <<= 6;
			wc |= ifs.get() & 0x3f;
		}
		*ptr++ = wc;
	}
	*length = static_cast<int>(reinterpret_cast<unsigned char*>(ptr) - buf);
	return buf;
}

int(*Getc)(FILE*) = 0;
int(*Ungetc)(int, FILE*) = 0;
int(*Feof)(FILE*) = 0;

static bool currentFileIsUtf8 = false;

wchar_t ReadFirstWchar(FILE *file)
{
	currentFileIsUtf8 = true;
	int c1 = Getc(file);
	if (Feof(file)) return 0xFEFF;
	if (c1 == 0xFF) {
		int c2 = Getc(file);
		if (Feof(file)) {
			Ungetc(c1, file);
			return 0xFEFF;
		}
		if (c2 == 0xFE) {
			currentFileIsUtf8 = false;
			return 0xFEFF;
		} else {
			Ungetc(c2, file);
			Ungetc(c1, file);
			return 0xFEFF;
		}
	}
	if (c1 != 0xEF) {
		Ungetc(c1, file);
	} else {
		int c2 = Getc(file);
		if (Feof(file)) {
			Ungetc(c1, file);
		} else if (c2 != 0xBB) {
			Ungetc(c2, file);
			Ungetc(c1, file);
		} else {
			int c3 = Getc(file);
			if (c3 != 0xBF || Feof(file)) {
				Ungetc(c2, file);
				Ungetc(c1, file);
			}
		}
	}
	return 0xFEFF;
}

wchar_t ReadNextWchar(FILE *file)
{
	if (currentFileIsUtf8) {
		int c = Getc(file);
		if (Feof(file)) return 0;
		if (c < 0x80) return c;
		if (c < 0xC0) {
			CLog::Add(CLog::Red, L"Can't decode UTF-8 (invalid character)");
			return 0;
		}
		size_t chars(0);
		wchar_t wc(0);
		if (c >= 0xFC) {
			wc = c & 0x01;
			chars = 5;
		} else if (c >= 0xF8) {
			wc = c & 0x03;
			chars = 4;
		} else if (c >= 0xF0) {
			wc = c & 0x07;
			chars = 3;
		} else if (c >= 0xE0) {
			wc = c & 0x0F;
			chars = 2;
		} else {
			wc = c & 0x1F;
			chars = 1;
		}
		for (; chars; --chars) {
			wc <<= 6;
			wc |= Getc(file) & 0x3f;
			if (Feof(file)) {
				CLog::Add(CLog::Red, L"Can't decode UTF-8 (end of input)");
				return 0;
			}
		}
		return wc;
	} else {
		int c1 = Getc(file);
		if (Feof(file)) return 0;
		int c2 = Getc(file);
		if (Feof(file)) return 0;
		return c1 | (c2 << 8);
	}
}

wchar_t* DecodeAndCopyHTML(const char *data, int *size)
{
	data -= 2;
	const unsigned char *dataPtr = reinterpret_cast<const unsigned char*>(data);
	if (*size >= 2 && dataPtr[0] == 0xFF && dataPtr[1] == 0xFE) {
		wchar_t *ret = reinterpret_cast<wchar_t*>(Malloc(*size));
		*size -= 2;
		memmove(ret, data + 2, *size);
		return ret;
	}
	if (*size >= 3 && dataPtr[0] == 0xEF && dataPtr[1] == 0xBB && dataPtr[2] == 0xBF) {
		dataPtr += 3;
		*size -= 3;
	}
	wchar_t *ret = reinterpret_cast<wchar_t*>(Malloc(*size * 2));
	wchar_t *writePtr = ret;
	for (size_t i = 0; i < *size;) {
		int c = dataPtr[i];
		++i;
		if (c < 0x80) {
			*writePtr++ = c;
			continue;
		}
		if (c < 0xC0) {
			CLog::Add(CLog::Red, L"Can't decode UTF-8 (invalid char)");
			break;
		}
		size_t chars(0);
		wchar_t wc(0);
		if (c >= 0xFC) {
			wc = c & 0x01;
			chars = 5;
		} else if (c >= 0xF8) {
			wc = c & 0x03;
			chars = 4;
		} else if (c >= 0xF0) {
			wc = c & 0x07;
			chars = 3;
		} else if (c >= 0xE0) {
			wc = c & 0x0F;
			chars = 2;
		} else {
			wc = c & 0x1F;
			chars = 1;
		}
		for (; chars; --chars, ++i) {
			if (i >= *size) {
				CLog::Add(CLog::Red, L"Can't decode UTF-8 in file (end of input)");
				break;
			}
			wc <<= 6;
			wc |= dataPtr[i] & 0x3f;
		}
		*writePtr++ = wc;
	}
	*size = reinterpret_cast<char*>(writePtr) - reinterpret_cast<char*>(ret);
	return ret;
}

bool FVector::IsInRange(const FVector &other, const double distance)
{
	double dx = other.x - x;
	double dy = other.y - y;
	double dz = other.z - z;
	return dx * dx + dy * dy + dz * dz <= distance * distance;
}
