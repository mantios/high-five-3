
#pragma once

#include <Common/CSharedData.h>

class CSharedItemData : public CSharedData {
public:
	/* 0x0040 */ unsigned char padding0x0040[0x0048 - 0x0040];
	/* 0x0048 */ UINT64 amount;
	/* 0x0050 */ unsigned char padding0x0050[0x0058 - 0x0050];
	/* 0x0058 */ UINT32 classId;
	/* 0x005C */ UINT32 itemType2;
	/* 0x0060 */ UINT32 bodyPart;
	/* 0x0064 */ UINT32 bless;
	/* 0x0068 */ UINT32 consumeType;
	/* 0x006C */ UINT32 damaged;
	/* 0x0070 */ UINT32 enchant;
	/* 0x0074 */ unsigned char padding0x0074[0x090 - 0x0074];
};

