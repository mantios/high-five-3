
#pragma once

#include <list>

template<class T>
class CSPointer {
public:
	CSPointer() : obj(0), type(0)
	{
	}

	__forceinline CSPointer(const CSPointer &other) : obj(other.obj), type(other.type)
	{
		if (!obj) return;
		obj->IncRef(__FILE__, __LINE__, type);
	}

	__forceinline explicit CSPointer(T *obj, int type = 9) : obj(obj), type(type)
	{
		if (!obj) return;
		obj->IncRef(__FILE__, __LINE__, type);
	}

	__forceinline ~CSPointer()
	{
		if (!obj) return;
		obj->DecRef(__FILE__, __LINE__, type);
	}

	__forceinline CSPointer& operator=(const CSPointer &other)
	{
		return Reset(other.obj, other.type);
	}

	__forceinline CSPointer& Reset()
	{
		if (obj) {
			obj->DecRef(__FILE__, __LINE__, type);
		}
		obj = 0;
		type = 0;
		return *this;
	}

	__forceinline CSPointer& Reset(T *obj_, UINT32 type_)
	{
		if (obj_) {
			obj_->IncRef(__FILE__, __LINE__, type_);
		}
		if (obj) {
			obj->DecRef(__FILE__, __LINE__, type);
		}
		obj = obj_;
		type = type_;
		return *this;
	}

	T& operator*()
	{
		return *obj;
	}

	T* operator->()
	{
		return obj;
	}

	operator T*()
	{
		return obj;
	}

	int GetType() const
	{
		return type;
	}

protected:
	T *obj;
	UINT32 type;
};

template<class T>
class CSPointerHelper {
public:
	CSPointerHelper(CSPointer<T> &pointer) : pointer(pointer)
	{
	}

	~CSPointerHelper()
	{
		pointer.Reset();
	}

	CSPointer<T>& GetCopy()
	{
		borrowed.push_back(CSPointer<T>(pointer));
		return borrowed.back();
	}

	T& operator*()
	{
		return *pointer;
	}

	T* operator->()
	{
		return pointer;
	}

	operator T*()
	{
		return pointer;
	}

	int GetType() const
	{
		return pointer.GetType();
	}

protected:
	CSPointer<T> &pointer;
	std::list<CSPointer<T> > borrowed;
};

template<class T>
class CSafePointer2 {
public:
	CSPointer<T> FindObjectSP();

protected:
	int objectId;
	int index;
};