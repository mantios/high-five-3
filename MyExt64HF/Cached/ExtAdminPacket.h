
#pragma once

namespace cached {

class CAdminSocket;

class ExtAdminPacket {
public:
	static void Init();
	static bool ExtHandler(class CAdminSocket *socket, const unsigned char *packet);
	static bool SetPremiumPacket(class CAdminSocket *socket, const unsigned char *packet);
};

} // namespace cached

