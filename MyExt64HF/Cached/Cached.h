
#pragma once

#include <windows.h>

namespace cached {

class CSocket;

class Cached {
public:
	static void Init();

	static CSocket* GetServerSocket();

protected:
	static void DisableSendMail();
	static void HookStart();
	static void ChangePaths();
	static void DontSetThreadCoreAffinity();
	static void InitUtf8Support();

	static HWND __cdecl CreateWindowEx(DWORD dwExStyle, LPCWSTR lpClassName, LPCWSTR lpWindowName, DWORD dwStyle, int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam);
	static void __cdecl StartHook(void *logger, int level, const char *fmt);
};

} // namespace cached

