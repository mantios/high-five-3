
#include <Cached/CUser.h>
#include <Common/Utils.h>
#include <Common/Enum.h>
#include <Common/CLog.h>
#include <Common/Config.h>

namespace cached {

void CUser::Init()
{
	WriteInstructionJmp(0x551658, FnPtr(CheckCharacterName));
}

bool CUser::CheckCharacterName(const wchar_t *name, int country, bool allowLongerNames)
{
	GUARDED;

	int len = wcslen(name);
	int maxLen = (allowLongerNames || country == COUNTRY_THAILAND) ? 24 : 16;
	if (len < 1 || len > maxLen) {
		CLog::Add(CLog::Red, L"invalid charname CheckCharacerName failed");
		return false;
	}
	for (size_t i = 0 ; i < len ; ++i) {
		if ((name[i] >= 'a' && name[i] <= 'z')
			|| (name[i] >= 'A' && name[i] <= 'Z')
			|| (name[i] >= '0' && name[i] <= '9')) {

			maxLen -= 1;
		} else {
			if (Config::Instance()->charsets->allowedCharacterNameLetters.count(name[i])) {
				if (name[i] >= 0x0530) {
					maxLen -= 2;
				} else {
					maxLen -= 1;
				}
			} else if (country == COUNTRY_KOREA) {
				if (name[i] >= 0xAC00 && name[i] <= 0xD7AF) { // korean letters
					maxLen -= 2;
				} else {
					return false;
				}
			} else if (country == COUNTRY_JAPAN || country == COUNTRY_TAIWAN || country == COUNTRY_CHINA) {
				if ((name[i] >= 0x3041 && name[i] <= 0x3096) // katakana
					|| (name[i] >= 0x30A1 && name[i] <= 0x30FC) // hiragana
					|| (name[i] >= 0x4E00 && name[i] <= 0x9FA5)) { // cjk unified ideographs

					maxLen -= 2;
				} else {
					return false;
				}
			} else if (country == COUNTRY_THAILAND) {
				if ((name[i] >= 0x0E01 && name[i] <= 0x0E3A) // thai unicode letters
					|| (name[i] >= 0xE3F && name[i] <= 0xE5B)) { // thai unicode letters
					maxLen -= 2;
				} else {
					return false;
				}
			} else if (country == COUNTRY_RUSSIA) {
				if (name[i] >= 0x0410 && name[i] <= 0x044F) { // cyrillic letters
					maxLen -= 1;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
		if (maxLen < 0) return false;
	}
	return true;
}

} // namespace

