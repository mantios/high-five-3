
#pragma once

namespace cached {

class ExtPacket {
public:
	static void Init();
	static bool Handler(class CSocket *socket, const unsigned char *packet);
	static bool ExtHandler(class CSocket *socket, const unsigned char *packet);
	static bool TestPacket(class CSocket *socket, const unsigned char *packet);
	static bool GetPremiumPacket(class CSocket *socket, const unsigned char *packet);
	static bool SetPremiumPacket(class CSocket *socket, const unsigned char *packet);
};

} // namespace cached

