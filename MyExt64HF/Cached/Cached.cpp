
#include <Cached/Cached.h>
#include <Cached/ExtPacket.h>
#include <Cached/ExtAdminPacket.h>
#include <Cached/CUser.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/Config.h>

namespace cached {

void Cached::Init()
{
	// Disable sending mail to NCsoft
	DisableSendMail();

	// Hook cached start
	HookStart();

	// Change CacheDScript to Script if configured to do so
	if (Config::Instance()->server->oneScriptDirectory) ChangePaths();

	// UTF-8 support
	InitUtf8Support();

	// Initialize custom packets
	ExtPacket::Init();

	// Initialize custom admin packets
	ExtAdminPacket::Init();

	// Disable setting thread core affinity
	DontSetThreadCoreAffinity();

	// Initialize user extensions
	CUser::Init();
}

CSocket* Cached::GetServerSocket()
{
	return *reinterpret_cast<CSocket**>(0x3B1CDC0);
}

void Cached::DisableSendMail()
{
	NOPMemory(0x46AFED, 5);
	NOPMemory(0x46C220, 5);
	NOPMemory(0x46C7B7, 5);
}

void Cached::HookStart()
{
	WriteInstructionCall(0x453FC2, reinterpret_cast<UINT32>(StartHook));
	WriteInstructionCall(0x453F08, reinterpret_cast<UINT32>(CreateWindowEx), 0x453F0E);
}

void Cached::ChangePaths()
{
	ReplaceString(0x614E48, L"CachedScript", L"Script");
	ReplaceString(0x614FB0, L"CachedScript", L"Script");
	ReplaceString(0x6150F8, L"CachedScript", L"Script");
	ReplaceString(0x653CE8, L"CacheDScript", L"Script");
	ReplaceString(0x653E58, L"CacheDScript", L"Script");
	ReplaceString(0x69A340, L"CacheDScript", L"Script");
}

HWND Cached::CreateWindowEx(DWORD dwExStyle, LPCWSTR lpClassName, LPCWSTR lpWindowName, DWORD dwStyle, int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam)
{
	// Just change window title
	std::wstring name(lpWindowName);
	name += L" - patched by MyExt64HF";

	// Call original CreateWindowEx
	return ::CreateWindowEx(dwExStyle, lpClassName, name.c_str(), dwStyle, X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
}

void Cached::StartHook(void *logger, int level, const char *fmt)
{
	// Original start log message
	reinterpret_cast<void(*)(void*, int, const char*)>(0x48B16C)(logger, level, fmt);

	// Add our log message
	CLog::Add(CLog::Blue, L"Patched by MyExt64HF");

	// Move old dumps to backup directory
	ShellExecute(0, L"open", L"cmd.exe", L"/C mkdir bak", 0, SW_HIDE);
	ShellExecute(0, L"open", L"cmd.exe", L"/C move LinError.txt.*.bak bak\\", 0, SW_HIDE);
	ShellExecute(0, L"open", L"cmd.exe", L"/C move minidump_*.dmp bak\\", 0, SW_HIDE);
}

void Cached::DontSetThreadCoreAffinity()
{
	NOPMemory(0x4830D1, 6); // let OS decide which thread will execute on which core
}

void Cached::InitUtf8Support()
{
	WriteInstructionCall(0x576281, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x57636A, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x5CA969, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x5CA9EE, FnPtr(ReadNextWchar));
}

} // namespace cached

