
#include <Cached/ExtPacket.h>
#include <Cached/CSocket.h>
#include <Cached/DBConn.h>
#include <time.h>
#include <Common/CLog.h>
#include <Common/Utils.h>
#include <Common/Enum.h>

namespace cached {

void ExtPacket::Init()
{
	WriteAddress(0x60D858 + 3, FnPtr(Handler));
}

bool ExtPacket::Handler(CSocket *socket, const unsigned char *packet)
{
	GUARDED;

	UINT16 opcode = 0;
	const unsigned char *packetData = Disassemble(packet, "h", &opcode);
	bool(*handler)(CSocket*, const unsigned char*);
	if (opcode > 0x102) {
		handler = reinterpret_cast<bool(**)(CSocket*, const unsigned char*)>(0x3B1C218)[0];
	} else if (opcode == 0x102) {
		handler = &ExtHandler;
	} else {
		handler = reinterpret_cast<bool(**)(CSocket*, const unsigned char*)>(0x3B1C220)[opcode];
	}
	return handler(socket, packetData);
}

bool ExtPacket::ExtHandler(CSocket *socket, const unsigned char *packet)
{
	UINT16 opcode = 0;
	const unsigned char *packetData = Disassemble(packet, "h", &opcode);
	switch (opcode) {
	case CDBExtPacket::REQUEST_TEST: return TestPacket(socket, packetData); break;
	case CDBExtPacket::REQUEST_GET_PREMIUM: return GetPremiumPacket(socket, packetData); break;
	case CDBExtPacket::REQUEST_SET_PREMIUM: return SetPremiumPacket(socket, packetData); break;
	default:
		CLog::Add(CLog::Red, L"Unknown ExtPacket opcode %04X", opcode);
		return false;
	}
}

bool ExtPacket::TestPacket(CSocket *socket, const unsigned char *packet)
{
	int i = 0;
	Disassemble(packet, "d", &i);
	CLog::Add(CLog::Blue, L"TestPacket request :) %d", i);
	socket->Send("chhd", 0xC0, 0x00AA, CDBExtPacket::REPLY_TEST, i);
	return false;
}

bool ExtPacket::GetPremiumPacket(CSocket *socket, const unsigned char *packet)
{
	int accountId = 0;
	Disassemble(packet, "d", &accountId);

	UINT32 expireTime = 0;

	DBConn conn;
	conn.Prepare(L"SELECT expire_time FROM dbo.premium_account WHERE account_id=?");
	conn.Bind(&expireTime);
	conn.BindParameter(accountId);
	if (!conn.Execute()) {
		CLog::Add(CLog::Red, L"Can't get premium expire time for account %d from DB", accountId);
		return false;
	}
	conn.Fetch();

	socket->Send("chhdd", 0xC0, 0x00AA, CDBExtPacket::REPLY_GET_PREMIUM, accountId, expireTime);

	return false;
}

bool ExtPacket::SetPremiumPacket(CSocket *socket, const unsigned char *packet)
{
	int accountId = 0;
	UINT32 expireTime = 0;
	Disassemble(packet, "dd", &accountId, &expireTime);

	DBConn conn;
	conn.Prepare(
		L"MERGE dbo.premium_account WITH (HOLDLOCK) "
		L"USING ("
			L"VALUES(?, ?)"
		L") AS new_row(account_id, expire_time) "
		L"ON dbo.premium_account.account_id = new_row.account_id "
		L"WHEN MATCHED THEN "
			L"UPDATE SET dbo.premium_account.expire_time = new_row.expire_time "
		L"WHEN NOT MATCHED THEN "
			L"INSERT(account_id, expire_time) "
			L"VALUES(new_row.account_id, new_row.expire_time);");
	conn.BindParameter(accountId);
	conn.BindParameter(expireTime);
	conn.Execute();

	socket->Send("chhdd", 0xC0, 0x00AA, CDBExtPacket::REPLY_SET_PREMIUM, accountId, expireTime);
	return false;
}

} // namespace cached

