
#pragma once

namespace cached {

class CSocket {
public:
	/* 0x0000 */ virtual ~CSocket() {}
	/* 0x0008 */ virtual void vfn0x0008() {}
	/* 0x0010 */ virtual void vfn0x0010() {}
	/* 0x0018 */ virtual void vfn0x0018() {}
	/* 0x0020 */ virtual void vfn0x0020() {}
	/* 0x0028 */ virtual void vfn0x0028() {}
	/* 0x0030 */ virtual void vfn0x0030() {}
	/* 0x0038 */ virtual void vfn0x0038() {}
	/* 0x0040 */ virtual void vfn0x0040() {}
	/* 0x0048 */ virtual void vfn0x0048() {}
	/* 0x0050 */ virtual void vfn0x0050() {}
	/* 0x0058 */ virtual void vfn0x0058() {}
	/* 0x0060 */ virtual void vfn0x0060() {}
	/* 0x0068 */ virtual void vfn0x0068() {}
	/* 0x0070 */ virtual void vfn0x0070() {}
	/* 0x0078 */ virtual void vfn0x0078() {}
	/* 0x0080 */ virtual void Send(const char *format, ...) {}

	/* 0x0008 */ unsigned char padding0x0008[0x0188 - 0x0008];
};

} // namespace cached

