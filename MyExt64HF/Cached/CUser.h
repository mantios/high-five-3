
#pragma once

namespace cached {

class CUser {
public:
	static void Init();

	static bool CheckCharacterName(const wchar_t *name, int country, bool allowLongerNames);
};

} // namespace cached

