
#include <Cached/ExtAdminPacket.h>
#include <Cached/CSocket.h>
#include <Cached/DBConn.h>
#include <Cached/Cached.h>
#include <Cached/CAdminSocket.h>
#include <time.h>
#include <Common/CLog.h>
#include <Common/Utils.h>
#include <Common/Enum.h>

namespace cached {

void ExtAdminPacket::Init()
{
	WriteMemoryBYTE(0x42A2C8, 0xA3);
	WriteMemoryQWORD(0x6DF700, FnPtr(ExtHandler));
}

bool ExtAdminPacket::ExtHandler(CAdminSocket *socket, const unsigned char *packet)
{
	UINT16 opcode = 0;
	const unsigned char *packetData = Disassemble(packet, "h", &opcode);

	if (socket->disabled) {
		socket->Send("chdd", 0xA2, opcode, 0, 0x1E);
		return false;
	}

	switch (opcode) {
	case 0x0000: return SetPremiumPacket(socket, packetData);
	default:
		CLog::Add(CLog::Red, L"Unknown ExtAdminPacket opcode %04X", opcode);
		return false;
	}
}

bool ExtAdminPacket::SetPremiumPacket(CAdminSocket *socket, const unsigned char *packet)
{
	int accountId = 0;
	UINT32 seconds = 0;
	Disassemble(packet, "dd", &accountId, &seconds);
	seconds += time(0);

	DBConn conn;
	conn.Prepare(
		L"MERGE dbo.premium_account WITH (HOLDLOCK) "
		L"USING ("
			L"VALUES(?, ?)"
		L") AS new_row(account_id, expire_time) "
		L"ON dbo.premium_account.account_id = new_row.account_id "
		L"WHEN MATCHED THEN "
			L"UPDATE SET dbo.premium_account.expire_time = new_row.expire_time "
		L"WHEN NOT MATCHED THEN "
			L"INSERT(account_id, expire_time) "
			L"VALUES(new_row.account_id, new_row.expire_time);");
	conn.BindParameter(accountId);
	conn.BindParameter(seconds);
	conn.Execute();

	if (CSocket *serverSocket = Cached::GetServerSocket()) serverSocket->Send("chhdd", 0xC0, 0x00AA, CDBExtPacket::REPLY_SET_PREMIUM, accountId, seconds);

	socket->Send("chd", 0xA2, 1, 0x0000);

	return false;
}

} // namespace cached

